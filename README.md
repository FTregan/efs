# Transcript/prise de notes  des épisodes Électronique From Scratch de Rancune

- ![Logo twitch](images/twitch.png) Chaine twitch : [https://www.twitch.tv/rancune_](https://www.twitch.tv/rancune_)
- ![Logo twitch](images/youtube.png) Chaine youtube (replay) :  https://www.youtube.com/channel/UCMucfdvN7-_WqJSL5PhLkng
- Mailing list : http://listes.lautre.net/cgi-bin/mailman/listinfo/efs
- IRC : _canal =_ **#efs** | _serveur =_ irc.libera.chat:6697 | _webchat =_ https://web.libera.chat/#efs


 
## Électronique From Scratch, épisode 1 : Les bases 

[<img src="images/efs_001/01_chat_polystyrene1.png" alt="EFS épisode 1" width="250"/>](efs_ep001.md)
[EFS épisode 1](efs_ep001.md)

## Électronique From Scratch, épisode 2 : Résistance(s)

[<img src="images/efs_002/01_chat_colonel.jpg" alt="EFS épisode 2" width="250"/>](efs_ep002.md)
[EFS épisode 2](efs_ep002.md)

## Électronique From Scratch, épisode 3 : Les ponts diviseurs

[<img src="images/efs_003/01_monorail_cat.jpg" alt="EFS épisode 3" width="250"/>](efs_ep003.md)
[EFS épisode 3](efs_ep003.md)

## Électronique From Scratch, épisode 4 : Parfois faut simuler !

[<img src="images/efs_004/01_chat_neo.png" alt="EFS épisode 4" width="250"/>](efs_ep004.md)
[EFS épisode 4](efs_ep004.md)

## Électronique From Scratch, épisode 5 : Le condensateur

[<img src="images/efs_005/01_chat_sandwich.png" alt="EFS épisode 5" width="250"/>](efs_ep005.md)
[EFS épisode 5](efs_ep005.md)

## Électronique From Scratch, épisode 6 : Les mystères du condo...

[<img src="images/efs_006/01_chat_sandwich.png" alt="EFS épisode 6" width="250"/>](efs_ep006.md)
[EFS épisode 6](efs_ep006.md) 

## Électronique From Scratch, épisode 7 : Ondule ton corps...

[<img src="images/efs_007/01_chat_ondule.png" alt="EFS épisode 7" width="250"/>](efs_ep007.md)
[EFS épisode 7](efs_ep007.md) 

## Électronique From Scratch, épisode 8 : Sinusoïdes et impédances complexes

[<img src="images/efs_008/01_chat_tequila.png" alt="EFS épisode 8" width="250"/>](efs_ep008.md)
[EFS épisode 8](efs_ep008.md) 

## Électronique From Scratch, épisode 9 : Les complexes (tome 2)

[<img src="images/efs_009/01_chat_plafond.png" alt="EFS épisode 9" width="250"/>](efs_ep009.md)
[EFS épisode 9](efs_ep009.md) 

## Électronique From Scratch, épisode 10 : Inductances et bobines

[<img src="images/efs_010/01_chat_bobine.png" alt="EFS épisode 10" width="250"/>](efs_ep010.md)
[EFS épisode 10](efs_ep010.md) 


## Électronique From Scratch, épisode 11 : Circuits RC & RL - Filtre Passe-bas

[<img src="images/efs_011/01_chat_cut.png" alt="EFS épisode 11" width="250"/>](efs_ep011.md)
[EFS épisode 11](efs_ep011.md) 


## Soirée Chill et Projets, épisode 9 : Le 555, ou pourquoi je m'en balance

[<img src="images/scp_009/01_chat_balance.png" alt="SCP épisode 9" width="250"/>](scp_ep009.md)
[SCP épisode 9](scp_ep009.md)  


## Électronique From Scratch, épisode 12 : RC et RLC

[<img src="images/efs_012/01_chat_noir.png" alt="EFS épisode 12" width="250"/>](efs_ep012.md)
[EFS épisode 12](efs_ep012.md)


## Électronique From Scratch, épisode 13 : Circuits LC et RLC Stay tuned !

[<img src="images/efs_013/01_chat_cri.png" alt="EFS épisode 13" width="250"/>](efs_ep013.md)
[EFS épisode 13](efs_ep013.md)


## Électronique From Scratch, épisode 14 : Semi-conducteurs et diodes

[<img src="images/efs_014/01_chat_tete_cote.png" alt="EFS épisode 14" width="250"/>](efs_ep014.md)
[EFS épisode 14](efs_ep014.md)


## Électronique From Scratch, épisode 15 : Les transistors bipolaires (1)

[<img src="images/efs_015/01_chat_triangle.png" alt="EFS épisode 15" width="250"/>](efs_ep015.md)
[EFS épisode 15](efs_ep015.md)
 

## Soirée Chill et Projets, épisode 12 : Premiers pas sur STM32

[<img src="images/scp_012/01_chat.png" alt="SCP épisode 12" width="250"/>](scp_ep012.md)
[SCP épisode 12](scp_ep012.md)  


 ## Électronique From Scratch, épisode 16 : Les transistors bipolaires (2)

[<img src="images/efs016/01_chat.png" alt="EFS épisode 16" width="250"/>](efs_ep016.md)
[EFS épisode 16](efs_ep016.md)  
 

## Soirée Chill et Projets, épisode 13 : STM32 - GPIO

[<img src="images/scp_013/01_chat.png" alt="SCP épisode 13" width="250"/>](scp_ep013.md)
[SCP épisode 13](scp_ep013.md)   [En cours...]
