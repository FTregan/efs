# EFS ep3 : Les ponts diviseurs

**Épisode 3 ( 10 juin 2022 )** 

<img src="images/efs_003/01_monorail_cat.jpg " alt="chat_colonel" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=y-_t8SlkgEQ
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1499915913

# Previously in EFS
## Résumé des épisodes précédents
[![youtube](images/youtube.png) (00:01:13)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=1m13s)   

- 3 types de composants de base à connaitre : Résistances, capacité (condensateurs) et inductances.

Aujourd'hui on continue sur les résistances. cf. [Episode 2](efs_ep002.md)

Les résistances sont des dipôles qui imposent, en tout temp, une contrainte linéaire entre le courant qui la traverse et la tension à ses bornes : la loi d'ohm.  
On aura toujours la relation : $`U=R \times i`$  
Attention à bien utiliser la convention récepteur pour utiliser les formules.  
![previously](images/efs_003/02_previously_ep3.png)

La résistance dissipe de la chaleur par effet joule, d'une puissance P, en Watt (W) :
$` P = U \times i  \Leftrightarrow  P = R \times i^2 \Leftrightarrow  P = \frac {U^2} R`$ 

Quand la puissance dissipée devient supérieure à sa puissance limite (donnée par le fabricant), la résistance brunit et fini par fumer et brûler. Une résistance traversante standard est donnée pour 1/4 W max.  
Les résistances réelles n'ont pas une valeur parfaite, elles ont une tolérance sur leur valeur fournies (10%, 5%, 1%,...)


3 paramètres entrent en jeu pour une résistance :
- la valeur de la résistance (en Ω)
- la puissance maximale qu'elle peut dissiper sans dommage (en W)
- sa tolérance, son incertitude sur la valeur (en %)


# Existe-t-il toutes les valeurs de résistances possibles

<img src="images/mario_non.png" alt="mario_non" width="150"/>

Si une résistance est données pour 100Ω avec une incertitude de 10%, une résistance donnée pour 101Ω n'a pas beaucoup de sens : elle est dans la tolérance de la première.  
![valeurs et tolérances](images/efs_003/03_valeur_et_tolerance_resistance.png)


Il existe donc des "séries" de résistances. On travaille par décade :  
1Ω →  10Ω → 100Ω -> 1k → 10k → 100k → 1M...

Par exemple la série E12 : il existe 12 valeurs entre 1Ω et 10Ω, 12 valeurs entre 10Ω et 100Ω, etc...

Les séries les plus courantes :
E6 (20%), E12 (10%), E24 (5%), E48 (2%), E96 (1%), E192 (0.5%)

la couleur de la dernière bague de la résistance donne la tolérance (argent = 10%, or = 5%, marron = 1%,...).

Les valeurs de la série E24 :  
100, 110, 120, 130, 150, 160, 180, 200, 220, 240, 270, 300, 330, 360, 390, 430, 470, 510, 560, 620, 680, 750, 820, 910

voir : https://www.positron-libre.com/cours/tableaux-formulaires/tableau-serie-resistance.php

Il est possible d'associer des composants pour "fabriquer" des valeurs dont on ne dispose pas.

# Résistances en série et en parallèle
[![youtube](images/youtube.png) (00:23:35)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=23m35s)   

## Résistances en série

![Résistances en série](images/efs_003/04_resistances_serie.png)

Pour obtenir la valeur de la résistance équivalente ($`R_{eq}`$) de plusieurs résistances connectées en série, on doit ajouter les valeurs de chaque résistances :

```math
\begin{aligned}
R_{eq} & = R_1 + R_2 + ... R_n  \\
& = \sum _{n=1} ^{\infty} R_n
\end{aligned}
```


## Résistances en parallèle

![Résistances en parallèle](images/efs_003/05_resistances_parallele.png)

Pour calculer la résistance équivalente de plusieurs résistances en parallèle, on passe par la notion de "conductance"  (l'inverse de la résistance), avec la relation :

```math
\begin{aligned}
\frac 1 R_{eq} & =  \frac 1 R_1 + \frac 1 R_2 + ... + \frac 1 R_n  \\
 & = \sum _{n=1} ^\infty \frac 1 R_n
\end{aligned}
```

Dans le cas particulier de 2 résistances en parallèle (R₁ et R₂), la formule de calcul directe de la résistance équivalente est :

```math
R_{eq} = \frac{R_1  R_2} {R_1+R_2}
```


## Validation par une mesure au multimètre

(+ explication de l'utilisation d'un multimètre pour mesurer des résistances )

[![youtube](images/youtube.png) (00:26:07)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=26m07s)   

## Démonstration de la formule pour 2 résistances en parallèle

[![youtube](images/youtube.png) (00:30:34)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=30m34s)   


![Résistances en parallèle](images/efs_003/06_resistances_parallele_demo.png)
 
```math

\begin{align*}
    loi\ des\ mailles &→  U = R_{eq} i = R_1 i_1= R_2 i_2 
    \Rightarrow 
    \left\{
        \begin{array}{ll} 
            i=\frac U {R_{eq}}\\ \\
            i_1=\frac U {R_1}\\ \\
            i_2=\frac U {R_2}\\
        \end{array}  
    \right.

    \\

    loi\ des\ nœuds &→ i = i_1 + i_2  
    \Leftrightarrow 
        \frac U {R_{eq}} = \frac U {R_1}  + \frac U {R_2}  \\

\end{align*} \\ \  \\

\Updownarrow \\ \  \\
\begin{align*}
    
    →\Big[\  \frac 1 {R_{eq}} &=\frac 1 {R_1}  + \frac 1 {R_2 }  \\

    \frac 1 {R_{eq}} &=\frac {R_2} {R_1R_2}  + \frac {R_1} {R_1R_2 } \\
    \frac 1 {R_{eq}} &=\frac {R_2+R_1} {R_1R_2} \\
    →\Big[\ {R_{eq}} &=\frac  {R_1R_2}{R_1+R_2} 
\end{align*} \\

```


# Le pont diviseur 
[![youtube](images/youtube.png) (00:35:21)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=35m21s)  


![Pont diviseur](images/efs_003/07_pont_diviseur.png)

Un pont diviseur est constitué de 2 résistances (R₁ et R₂) auxquelles on branche une tension $`U_{in}`$ (par exemple 12V).   
On s'intéresse à la tension $`U_{out}`$ aux bornes de R₂ qu'on souhaiterait abaisser, par exemple à 5V. 

Attention, si $`i_{out}`$ est différent de 0, on ne peut pas parler de "résistances en série" car un courant "sort".  
Pour la démonstration de la formule du pont diviseur, voir cette vidéo ou bien le transcript de l' [épisode 2](efs_ep002.md)




## Démonstration de la formule du pont diviseur 
[![youtube](images/youtube.png) (00:38:51)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=38m51s)

![](images/efs_003/08_pont_diviseur_demo1.png) 

Hypothèse de travail : $`i_{out}=0`$  (pont divideur non chargé) :  
![](images/efs_003/09_pont_diviseur_demo2.png)


_**Rappel sur les lois de Kirchhoff** (loi des mailles et  loi des nœuds) [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Lois_de_Kirchhoff) :_ 
- _le transcript de l'[épisode 1](efs_ep001.md)_
- _le rappel vidéo de cet épisode 3 : 
[![youtube](images/youtube.png) (00:44:36)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=44m36s)_



L'hypothèse de travail pour le calcul de $`U_{out}`$ était un pont diviseur "à vide" ou  "non chargé" : $`i_{out}=0`$

Si notre pont diviseur alimente un raspberry Pi, par exemple, ce dernier "tire du courant" (environ 250mA).  On ne respecte donc pas l'hypothèse de $`i_{out}=0`$ : notre pont est "chargé"  
![](images/efs_003/10_chargez.png)


Pour que ça fonctionne, il faut donc ne pas "trop violer" l'hypothèse. Le courant traversant i₂ doit donc être très supérieur au courant de charge pris par le raspberry pi.

Quand on calcul un pont diviseur, on regarde donc un ratio de R₁ et R₂ tel que :

```math
U_{in} \frac {R_2} {R_1+R_2} = U_{out} = 5V
```

Mais également  R₁+R₂ doit etre choisi pour que i₂ soit très supérieur à $`I_{out}`$ (250mA dans notre exemple). On prend en général i₂ environ 10 fois supérieur au $`I_{out}`$ souhaité. Ici $` 250mA \times 10 = 2.5A `$.  
Cette intensité est "perdue" pour la charge (le raspberry) et fait chauffer les résistances. ($` 12V \times 2.5A = 30W `$ de perte ) 

**Ce montage n'est absolument pas efficace pour alimenter une charge importante comme un raspberry pi !**


Comme on sait que le raspberry pi consomme 250mA sous une tension de 5V, la loi d'ohm nous dit qu'on peut grosso modo le considérer comme une "résistance" de $` \frac 5 {250.10^{-3}} = 20Ω `$  
la résistance équivalente ($`R_{eq}`$) à R₂ et Rpi en parallèle peut être calculée. Le pont diviseur composé de R₁ et $`R_{eq}`$ est ainsi un nouveau pont diviseur considéré comme non chargé.


Pour voir la variation de la tension d'un pont diviseur en fonction de la variation d'une "résistance de charge", voir les [simulations ngspice du pont diviseur chargé](https://gitlab.com/mac_call/ngspice/-/tree/master/simulation_exemples) (faites suite 
à l'épisode 4 qui introduit Ngspice)  :
![pont diviseur en fonction de la charge](images/efs_003/11_pont_diviseur_simu_ngspice.png)


## Question : Pourquoi ne pas mettre juste une résistance avant le raspberry pi ?
[![youtube](images/youtube.png) (01:09:25)](https://www.youtube.com/watch?v=y-_t8SlkgEQ&t=1h9m25s)  

![Pi à la place de R₂ ?](images/efs_003/12_question_resistance_seule.png)

La "résistance équivalente" au Raspberry varie en fonction de son fonctionnement, on aurait donc un Req variable, donc une tension variable au borne du Pi qui peut sortir des branches de fonctionnement.

De plus au moment ou on branche la tension Vin, il n'y a pas de courant dans R₁, donc U₁=0, donc on a 12V directement aux bornes du Pi pendant quelques micro secondes, ce qui risque de l'abimer.

**Ce n'est donc pas une bonne solution.**

# Apparté contre intuitive
## Tension, interrupteurs et résistances
**Que se passe-t-il dans un circuit simple, au niveau des tensions, quand on ferme un interrupteur ?**

Prenons un circuit composé d'un générateur, d'une résistance et d'un interrupteur (switch).

- La différence de potentiel aux bornes du générateur est notée U et vaut 12V
- La différence de potentiel aux bornes de la résistance est notée Ur
- La différence de potentiel aux bornes du switch est notée Us


### Interrupteur ouvert
![circuit ouvert](images/efs_003/13_switch_ouvert.png)

Dans un circuit ouvert, aucun courant ne circule (i=0). La loi d'ohm nous dit donc qu'aux bornes de la résistance, la différence de potentiel $` U = Ri = R \times 0 = 0V `$  
D'après la loi des mailles $` U = 12V = Ur + Us = 0 + Us = Us `$

Donc les 12V du générateur se retrouvent aux bornes du switch.


### Interrupteur fermé
![circuit fermé](images/efs_003/14_switch_ferme.png)

Quand on ferme le circuit, le courant peut circuler (i différent de 0). Le switch étant fermé, il se comporte comme un simple conducteur, il n' y a donc aucune différence de potentiel à ses bornes : $` Us = 0 `$

D'après la loi des mailles $` U = 12V = Ur + Us = Ur + 0 = Ur `$  
Donc les 12V du générateur se retrouvent aux bornes de la résistance.


---

![fin](images/thats_all_folks_bugs.png)

Le [prochain épisode](efs_ep004.md) nous apprendra à simuler des circuits électriques avec NGspice.



