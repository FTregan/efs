TITRE# EFS ep4 : Parfois faut simuler !

**Épisode 4 ( 17 juin 2022 )** 

<img src="images/efs_004/01_chat_neo.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=BE13NAfmzYg
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1506344115

# Previously in EFS
[![youtube](images/youtube.png) (00:01:18)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=1m18s)  

## Le pont diviseur

![Pont diviseur à vide](images/efs_004/02_pont_diviseur.png)

La formule de la tension de sortie ( $` U_s `$ ) du pont diviseur à vide ⇔ non chargé ⇔  i=0 est :

$`
U_{s}  = U_{e} \frac {R_2} {R_1+R_2}
`$

En cas d'alimentation d'une charge i≠0, on doit donc esayer d'avoir l'intensité i environ 10 fois inférieure à l'intensité dans R₂ pour ne pas trop écrouler la tension de sortie du pont.

Le pont diviseur n'est absolument pas efficace pour alimenter une charge en courant. il y a 10 fois plus de courant "perdu pour la charge" et passant par R₂, que de courant "efficace" (i) utilisé par la charge.

Le pont diviseur est utile uniquement si utilisé à vide ou presque. Ou encore pour mesurer quelquechose , ce qui nous amène à un nouveau composant...


# Le potentiomètre
[![youtube](images/youtube.png) (00:07:00)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=7m0s) 

![principe du potentiomètre](images/efs_004/03_potentiometre_principe.png)

Le potentiomètre est une résistance comportant un curseur, un index, qui peut se déplacer d'un bout à l'autre de l'élément résistif. Entre une patte de la résistance et le curseur, la résistance varie donc en fonction de la position de l'index sur la résistance.

l'ensemble est un pont diviseur réglable.

Il en existe biens entendu de toutes les tailles, formes et qualités (rotatifs, à glissière, motorisés,...):

 ![principe du potentiomètre](images/efs_004/04_potentiometre1.png)
 ![principe du potentiomètre](images/efs_004/05_potentiometre2.png)

 ![principe du potentiomètre](images/efs_004/06_potentiometre3.png)


Les potentiomètres se caractérisent par : 
- leur résistance totale
- la tolérance de cette résistance
- la puissance maximale dissipée
- le nombre de tours permettant le réglage (1 tour, 1O tours,...)
- le sens de rotation (cw = clockwise, dans le sens des aiguilles d'une montre, et ccw = counter clockwise dans le sens inverse)
- la répartition (linéaire, log, inverse log...) c'est la courbe de variation de résistance par rapport à l'angle de rotation de l'axe. la plupart sont linéaires, mais pour l'audio, le logarithme est mieux adapté (le volume sonore suit une progression logarithmique)  
![courbe potentiometre](images/efs_004/07_potentiometre_log_courbe.png)

Le potentiomètre peut s'utiliser avec les 3 pattes quand on a besoin d'un pont diviseur.   
Mais si on a juste besoin d'une résistance variable, on peut n'utiliser que 2 pattes. En général, on relie alors la patte inutilisée au curseur : en électronique on n'aime pas ce qui reste "en l'air", ça peut faire antenne ou récupérer des parasites.


# Le pont de Wheatstone  [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Pont_de_Wheatstone)
[![youtube](images/youtube.png) (00:30:19)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=30m19s)  

## Présentation
[![youtube](images/youtube.png) (00:32:58)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=32m58s)  

![schéma pont de Wheatstone](images/efs_004/08_pont_wheatstone.png)

Le pont de Wheatstone est constitué de 4 résistances en H : 2 ponts diviseurs en parallèle.

On s'intéresse à la tension de sortie Us entre les sorties de chaque pont.

## La jauge de contrainte
[![youtube](images/youtube.png) (00:35:38)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=35m38s)  

Présentons une résistance particulière : la jauge de contrainte 

![Jauge de contrainte](images/efs_004/09_jauge_contrainte.png)

Cette jauge mesure la déformation d'un matériaux sur laquelle elle est "collée".
Si un fil fin est étiré, sa résisatnce augmente (il devient plus étroit).  
C'est le principe de la jauge de contrainte dont la résistance va varier en fonction de la contrainte.
C'est le composant utilisé dans les pèses-personnes électriques.


## Le rapport avec le pont de Wheatstone ?
[![youtube](images/youtube.png) (00:39:25)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=39m25s)  

La résistance de la jauge de contrainte va certes varier en fonction de la contrainte, mais aussi en fonction de la température et de plein de paramètres qui ne sont pas ceux qu'on veut mesurer.  
![](images/caramba_encore_rate1.png)

Heureusement le pont de Wheatstone vient à la rescousse.

### Calcul de la tension du pont
![](images/efs_004/10_pont_equilibre.png)

```math
\left\{
    \begin{array}{}
    \begin{align*}
        U_{R_2} &= \frac {R_2}{R_1+R_2}U \\  
        \\
        U_{R'_2} &= \frac {R'_2}{R'_1+R'_2}U` \\
        \\
        U_{out} &= U_{R_2} - U_{R'_2}  \\
        \end{align*}
    \end{array}{}
\right. \\
\ \\ 
U_{out} = U \underbrace { (\frac {R_2}{R_1+R_2} -\frac {R'_2}{R'_1+R'_2}) } _{=0\ si\ R₁ = R'₁\  et\  R₂ = R'₂}\\
```

### Équilibre et déséquilibre du pont
- Si R₁ = R'₁ et R₂ = R'₂ $`  \Rightarrow Uout = 0`$ : **le pont est dit "équilibré"**

- Si R₁ = R'₁ mais que R₂ légèrement ≠ R'₂, Uout n'est plus nul. Le pont se déséquilibre.  
Ça nous permet de faire des mesures différentielles : on mesure comment évolue la différence entre R₂ et R'₂.


### Avec les jauges de contrainte
![](images/efs_004/11_wheatstone_jauge.png)
 
Remplaçons R₂ et R'₂ par 2 jauges de contraintes, l'une (JC₁) mesurant la contrainte, et l'autre (JC₂) identique mais ne recevant aucune contrainte.  
On a ainsi une jauge de mesure (JC₁) et une autre de référence (JC₂) dont l'évolution suivra la jauge de mesure (même température etc...) SAUF pour la mesure de la contrainte.

Le déséquilibre du pont ne se fera que par différentiel de contrainte, les autres paramètres seront compensés.

Par ailleurs, vu que R₁ et R'₁ ne sont pas strictement identiques à cause de leur tolérance de construction, on peut remplacer R'₁ par un potentiomètre qui permettra de régler l'équilibrage du pont précisemment "à vide".

**Au final, la variation de la tension V ne dépendra que de la contrainte appliquée sur JC₁. Sauvé !**

La courbe d'évolution de la tension d'un pont de Wheatstone autour de sa zone d'équilibre a été simulée avec ngspice.  

Pour visualiser la variation de la tension de sortie d'un pont de Wheatstone autour de son équilibre, voir les [simulations ngspice du pont de wheatstone](https://gitlab.com/mac_call/ngspice/-/tree/master/simulation_exemples) (La simulation avec NGspice est vue plus loin dans cet épisode)  :  
![pont diviseur en fonction de la charge](images/efs_004/14_wheatstone_simu_kicad.png)  
L'équilibre du pont est représenté par le curseur en pointillés blanc (U=0V)



## D'autres capteurs résistifs existent
[![youtube](images/youtube.png) (00:48:48)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=48m47s)  

### LDR ou photorésistance
La LDR est une résistance dont la valeur varie avec la luminosité. Elle peut s'utiliser avec un simple pont diviseur, éventuellement avec un pont de Wheatstone.  
 ![photorésistance](images/efs_004/12_ldr.png)

### CTN ou thermistance
La termistance est une résistance dont la valeur évolue avec la température. Il en existe 2 grands types : 
- à coefficient positif : la résistance augmente avec la température
- à coefficient négatif : la résistance diminue avec la température  

 ![tehrmistor](images/efs_004/13_ctn.png)

Ce sont des résistances dont on a gardé (voire accentuer) le défaut naturel de construction qui fait que la température influence la résistance.

---

# Simulation de circuits analogiques avec NGSpice
[![youtube](images/youtube.png) (00:54:58)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=54m58s)  

SPICE a été développé à l'université de Berkeley au début des années 70, sous licence BSD [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/SPICE_(logiciel)). La dernière version date de 1993   
Il y a eu des clônes commerciaux, notamment LTSpice développé par Analog, distribué gratuitement et tournant sous windows avec interface graphique. il est donc très utilisé chez les électroniciens amateurs.    
Ngspice est l'évolution moderne et vraiment libre de Spice. Il s'utilise en ligne de commande avec des fichiers texte, mais il existe des GUIs.   
Il est notamment possible d'utiliser Ngspice avec kicad [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/KiCad))


## Les principaux types de simulations de ngspice
3 types de simulations principales

- OP (Operating Point) : courant continu, pas de variation une fois que le circuit a pris ses valeurs.

- AC :  courant changeant :  point de fonctionnement et on oscille autour du point de fonctionnement. sources de tension sinusoidales, etc... 

- Transient : phénomènes transitoires (non répétitif)  Par exemple : La charge du condensateur ne se passe qu'une fois

## Exemple simple de définition d'un circuit ( nets et composants )
Le principe est de définir, dans un fichier texte, la liste des composants (un par ligne) avec son type, un nom
/label/numero, ses caractéristiques et les "équipotentielles" (les nets) auxquels chacune de ses pattes est reliée.  
Le nommage des nets est libre (sauf 0 qui est obligatoire et est le potentiel choisit comme référence : 0V).  

Sur le shéma ci-dessous, les nets sont entourés en rouge, et sont nommés A, B et 0.   Chaque composant (dipôles entourés en bleu ici) relie 2 nets.   
Attention, à l'ordre des pattes dans la définition des composants, même si pour la résistance, ça ne change rien.  

![](images/efs_004/15_schema_spice.png)

-  V1 source de tension (V) numéro 1  reliée entre le nœud A et le nœud 0 : valeur = tension continue de 10V
-  R1 Résistance (R) numéro 1 reliée entre le nœud A et le noeud B ; valeur 50Ω
-  R2 Résistance (R) numéro 2 reliée entre le nœud B et le noeud 0 ; valeur 30Ω

Le nœud 0 est obligatoire dans spîce et indique la référence de tension (potentiel 0).

`.title` est en général facultatif devant le titre, par contre `.end` est obligatoire à la fin du fichier.

```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30
.end

```

## Documentation ngspice 
[![youtube](images/youtube.png) (01:23:11)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=1h23m11s)

- [site officiel NGSpice](http://ngspice.sourceforge.net)
- [documentation NGspice](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml)
  - Les éléments de circuits disponibles : [2.1.3](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#magicparlabel-283)
 

### Résistance (R)
doc :   [3.3.1](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#subsec_Resistors)
```
RXXXXXXX n+ n- <resistance|r=>value <ac=val> <m=val> <scale=val> 
+ <temp=val> <dtemp=val> <tc1=val> <tc2=val>
+ <noisy=0|1>
```

### Source de tension (V)
doc  : [4.1](http://ngspice.sourceforge.net/docs/ngspice-html-manual/manual.xhtml#sec_Independent_Sources_for) 

```
VXXXXXXX N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
IYYYYYYY N+ N- <<DC> DC/TRAN VALUE> <AC <ACMAG <ACPHASE>>>
+ <DISTOF1 <F1MAG <F1PHASE>>> <DISTOF2 <F2MAG <F2PHASE>>>
```

## Exécution d'une simulation ngspice
[![youtube](images/youtube.png) (01:31:00)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=1h31m00s)

### chargement du fichier
- Exécuter directement le fichier contenant la définition du circuit :
```
ngspice <nom_du_fichier>

```

- **OU** charger le fichier dans la CLI ngspice avec `source`
 
```
ngspice                         
[...]
ngspice 1 -> source <nom_du_fichier.cir>
```


### Lancement analyse en point de fonctionnement (OP)
#### Manuellement dans la CLI ngspice
```
op

Doing analysis at TEMP = 27.000000 and TNOM = 27.000000
```

La simu est effectuée, on peut imprimer les résultats. Par exemple la tension au noeud B avec `print B`  ou encore `print all` pour imprimer toutes les valeurs disponibles.
`display`  pour connaitre la nature des éléments mesurés (on voit qu'on peut avoir accès au courant de l'alimentation)

```
print B 
b=  3.750000e+00
print all
a = 1.000000e+01
b = 3.750000e+00
v1#branch = -1.25000e-01
ngspice 3 -> display
Here are the vectors currently active:

Title: Diviseur de tension
Name: op1 (Operating Point)
Date: Wed Jun 22 19:24:38  2022

    a                   : voltage, real, 1 long [default scale]
    b                   : voltage, real, 1 long
    v1#branch           : current, real, 1 long

```




#### Automatiquement en ajoutant des "contrôles" dans le fichier .cir

Il est possible d'ajouter les "contrôles" (les mesures à effectuer) dans le fichier de simu. ils seront exécuter au chargement du fichier automatiquement. (plus pratique)
```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30

.control
op
print B
.endc

.end

```


**Sortie : **

```
Circuit: Diviseur de tension

Doing analysis at TEMP = 27.000000 and TNOM = 27.000000


No. of Data Rows : 1
b = 3.750000e+00
```
#### Test de 'charge' du pont diviseur
On peut vérifier en ajoutant (sous R2 ...) une résistance de charge "élevée : `Rcharge B 0 10k`  que la tension du pont diviseur ne change pas beaucoup.

Par contre avec une charge "faible" `Rcharge B 0 10` la tension s'écroule.


### Simulation 'transient'
[![youtube](images/youtube.png) (01:49:49)](https://www.youtube.com/watch?v=BE13NAfmzYg&t=1h49m49s)

- la source de tension envoie un pulse de 0 à 5V au bout de 10ms. temps de montée et de descente : 1µs , durée de la pulsation 1s. Il est possible ensuite d'indiquer une période avant de recommencer)
- lancement d'une analyse 'transient' entre 50µs et 50ms  (avant la fin du pulse donc)
```

.title Diviseur de tension
V1 A 0 dc 0 PULSE (0 5 10m 1u 1u 1 )
R1 A B 50
R2 B 0 30

.control
  tran 50u 50m
  plot A B
.endc

.end

```

**Sortie : (graph)**
![Tension d'un pont diviseur selon variation de la charge](images/efs_004/16_graph_transient_pont_diviseur_simple.png)


###  Variation de valeur d'un composant
- La valeur de la résistance de charge du pont diviseur varie de 100Ω à 5Ω par pas de -10Ω
- Un calcul OP est fait à chaque changement et est enregistré dans un fichier `out.txt`
```

.title Diviseur de tension
V1 A 0 dc 10
R1 A B 50
R2 B 0 30
Rcharge B 0 500

.control
set filetype=ascii
let start_r = 100
let stop_r = 5
let delta_r = 10
let r = start_r
while r ge stop_r
  alter Rcharge r
  op
  write out.txt B
  set appendwrite
  let r = r - delta_r
end
.endc

.end


```


**Sortie : (extrait du fichier out.txt)**
```
Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    3.157894736842105e+00

Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    3.103448275862069e+00


[...]


Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    1.935483870967742e+00

Title: Diviseur de tension
Date: Wed Jun 22 19:38:53  2022
Plotname: Operating Point
Flags: real
No. Variables: 2
No. Points: 1
Variables:
    0   a   voltage
    1   b   voltage
Values:
 0  1.000000000000000e+01
    1.304347826086957e+00
	
```

---

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep005.md) nous attaquerons le condensateur.