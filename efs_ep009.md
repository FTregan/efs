# EFS ep9 : Les complexes (tome 2)

**Épisode 9 ( 22 juillet 2022 )** 

<img src="images/efs_009/01_chat_plafond.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=AaR9CmPYiqM
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1539448826

# Previously in EFS
[![youtube](images/youtube.png) (00:05:30)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=5m30s)  

## sinusoïdes
Nous avions vu qu'un signal de forme quelconque (mais périodique) peut être ramené à une somme de sinusoïdes :

```math
\sum _{\omega} \underbrace{A} _{Amplitude} cos(\underbrace{\omega} _{pulsation}t + \underbrace{\phi}_{phase\ à\ l'orgine})
```

On se ramène à un signal de base : la sinusoide.

Prenons un circuit classique auquel on injecte un signal caractérisé par une amplitude, une pulsation et une phase :[A,ω,ϕ].  
Généralement ce qui en sort, c'est un signal dont l'amplitude et la phase ont pu changer, mais dont la pulsation n'a pas changé :[A',ω,ϕ'].

On représentera donc les sinusoïdes par 2 paramètres au lieu de 3 : [A,ϕ], on oublie la pulsation.
- A est une valeur (tension, intensité...)
- ϕ est un angle (en radians)

## Complexes
Les mathématiques nous disent que les nombres complexes sont un bon outil pour représenter une valeur et un angle.

Les nombres réels sont tous sur un axe horizontal, les imaginaires purs sont sur un axe vertical. Ce ne sont pas des réels car j²=-1, j n'est pas réel, il n'existe pas, c'est un imaginaire.

### représentation arithmétique
Un nombre complexe, par exemple 2+3j a une partie réelle (sur les abcisses) de 2 et une partie imaginaire (sur les ordonnées) de 3.  
Les nombres complexes peuvent s'écrire sous la forme a+bj

### représentation géométrique
Il y a une autre façon, "géométrique" de représenter un nombre sur le plan complexe. Avec un angle $`\theta`$ et une distance $`\rho`$

### équivalences
On a l'équivalence  :  
$`a + b j  =  \rho e^{j\theta} `$ 


De plus on se rapelle que la formule d'Euler [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Formule_d'Euler) nous dit que :  
$`e^{j\theta} = \cos \theta + j \sin \theta`$  

![](images/efs_009/02_previously.png)

- a+bj est une forme qui est pratique pour les additions (il suffit d'additionner les parties réelles entre elles et les parties imaginaires entre elles)
- $`\rho e^{j\theta} `$ est pratique pour les multiplications : on multiplie les modules ($`\rho`$) et on ajoute les arguments ($`\theta `$)

### Pour les informaticiens
Pour les informaticiens, un nombre complexe est simplement une classe ou une struct avec 2 membres : $`[\rho ,\theta ] `$  
Cette classe a :
- une opération "multiplier" qui renvoie :  
$`[\rho ,\theta ] \times [\rho' ,\theta' ] = [\rho\rho' ,\theta+\theta' ] `$
- une opération "diviser" qui renvoie :  
$` \frac {[\rho ,\theta ]} {[\rho' ,\theta' ]} = [\frac \rho {\rho'} ,\theta-\theta' ] `$


 


# Loi d'ohm complexe

[![youtube](images/youtube.png) (00:27:10)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=27m10s)

Plutôt que de manipuler $`A\cos(\omega t + \phi)`$ On va plutôt  manipuler l'objet $`[A,\phi]`$ qui se trouve être similaire à un complexe. $`\omega`$ n'est pas repris, il sera une variable dans les expressions A et $`\phi`$

La loi d'ohm régit tout, en remplaçant la résistance par une impédance complexe. 

## Circuit avec un condensateur
[![youtube](images/youtube.png) (00:28:08)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=28m8s)

Prenons le circuit suivant :   
![](images/efs_009/03_calcul_condensateur.png)

**RAPPEL la notation complexe s'entend toujours avec des sinusoïdes.**

- un générateur fournit une tension sinusoïdale de 220V, sans déphasage [220,0]. 
- un condensateur "reçoit" cette tension. son impédance est $`[\frac {1}{C\omega}, -\frac {\pi}{2}]`$

On applique la loi d'ohm complexe :  
```math
\begin{align*}

\underline i &= \frac {\underline U}{\underline Z} \\
\ \\
\underline i &= \frac {[220,0]}{[\frac 1 {C \omega}, - \frac \pi 2]} \\
\ \\
\underline i &= [ \frac {220}{\frac 1 {C \omega}} , 0 - (- \frac \pi 2)] \\ \ \\
\underline i &= [ 220C \omega ,  \frac \pi 2]

\end{align*}
```

##  Circuit avec une résistance
[![youtube](images/youtube.png) (00:33:21)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=33m21s)

![](images/efs_009/04_calcul_resistance.png)

- un générateur fournit une tension sinusoïdale de 14V, sans déphasage [14,0]. 
- une résistance "reçoit" cette tension. son impédance est $`[R, 0]`$

On applique la loi d'ohm complexe :  
```math
\begin{align*}

\underline i &= \frac {\underline U}{\underline Z} \\
\ \\
\underline i &= \frac {[14,0]}{[R,0]} \\
\ \\
\underline i &= [ \frac {14}{R} , 0 - 0] \\ \ \\
\underline i &= [ \frac {14}{R} , 0] 

\end{align*}

```

On retrouve le $`i=\frac U R`$ sans déphasage, c'est cohérent avec la loi d'ohm simple pour la résistance.  
Les complexes sont juste une manière d'introduire le retard ou l'avance en plus.

Dans le plan complexe, 
- les résistances sont sur les abcisses
- Les condensateurs sont sur les ordonnées négatives (angle de {$`-\frac\pi 2 `$)
- Les bobines sont sur les ordonnées positives (on verra plus tard que leur déphasage est {$`+\frac\pi 2 `$)

![](images/efs_009/05_plan_c.png)


# Filtres RC
[![youtube](images/youtube.png) (01:00:36)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=1h0m36s)  

## Avé les main(g)s 
Le condensateur a une impédance :
- $`Z_C=\frac 1 {jC\omega} = -\frac j {C\omega}`$
- $`Z_C=[\frac 1 {C\omega} , -\frac \pi 2]`$
  
Si F est très grand, Zc tend vers 0 , le condensateur se comporte comme un conducteur

Si F est très petit Zc tend vers l'infini, le condensateur se comporte comme un circuit ouvert.

### Passe bas
Dans le cas d'un montage RC, quand on regarde Uc   
![](images/efs_009/06_circuit_passe_bas.png)


À très grande fréquence, le condensateur se comporte comme un fil, Us=0

À très basse fréquence, le condensateur se comporte comme un circuit ouvert, Us = Ue

Ceci est un filtre passe-bas : il laisse passer les basses fréquences, et tue les hautes fréquences qui sont directement ramenées à la masse.


### Passe haut
Dans le cas d'un montage CR, quand on regarde Ur   
![](images/efs_009/07_filtre_passe_haut.png)


À très grande fréquence, le condensateur se comporte comme un fil, Us=Ue

À très basse fréquence, le condensateur se comporte comme un circuit ouvert, Ur = 0 (la résistance n'est plus connectée)

Ceci est un filtre passe-haut : il laisse passer les hautes fréquences et "déconnecte" les basses fréquences. 

### Les autres filtres
Passe bande et rejecteur de bande :  
![](images/efs_009/08_passe_bande_rejecteur.png)



## Calculons maintenant
[![youtube](images/youtube.png) (01:20:44)](https://www.youtube.com/watch?v=AaR9CmPYiqM&t=1h20m44s) 
### Passe bas
![](images/efs_009/09_RC_passe_bas.png)

On va faire une étude en fréquence

On cherche Uc (tension de sortie au bornes du condensateur)  alimenté via une résistane R par une Tension Uin

On est en présence d'un pont diviseur de tension :  
![](images/efs_009/10_pont_div_impedance.png)

Les impédances des 2 éléments du pont sont :
  - $`\underline{Z_R} = [R ; 0] = R `$
  - $`\underline{Z_C} = \frac 1 {jx\omega} = [\frac 1 {C\omega} ; -\frac {\pi} 2] = R `$

La formule "complexe" du pont diviseur est :

```math 
\underline{U_c}= \frac {\underline{Z_c}} {\underline{Z_R} + \underline{Z_C}} \underline{U_{in}} 
\\ \  \\
\frac {\underline{U_c}} {\underline{U_{in}}} = \frac {\frac 1 {jC\omega} } {R + \frac 1 {jC\omega}}  

\\ \  \\
\frac {\underline{U_c}} {\underline{U_{in}}} = \frac {1} {RjC\omega +1 }  
```

Sous forme "géométrique", cette formule sera plus facile à utiliser.
Après calcul (non détaillé ici) on obtiendra :
```math
\frac {\underline{U_c}} {\underline{U_{in}}} = \frac {1} {\sqrt{1+ (\omega RC)²}} e^{j\arctan (-\omega RC)}  
```

avec : 
- amplification (gain) : $` \frac {1} {\sqrt{1+ (\omega RC)²}} `$
- déphasage : $`\arctan (-\omega RC) `$


Le diagramme de bode résultant de ces 2 formules :   
![](images/efs_009/11_bode.png)


0 db correspond à un gain de 1 : Uc=Uin  

En dessous d'une certaine fréquence, dite fréquence de coupure, on a un gain de 1 : Uc=Uin, les fréquences passent sans atténuation.   
Au delà de cette fréquence de coupure, le gain chute, pour finir, dans les très hautes fréquences, par ne plus rien laisser passer.

En ce qui concerne le déphasage entre Uc et Uin, dans les basses féquences, il est nul. À la fréquence de coupure, il est de $`-\frac \pi 4`$ , et fini, dans les hautes fréquences, par être de $`- \frac \pi 2`$, ce qui est le déphasage caratéristique des condensateurs.

La fréquence de coupure correspond à la fréquence pour laquelle la sortie est de 70% la valeur de celle de l'entrée (-3db).
Pour les fréquences suivantes, le gain se casse la figure continuellement ensuite.

On reviendra précisemment sur les décibels au prochain épisode.

## Revenons sur le gain

Un filtre quel qu'il soit, fonctionne toujours de la même manière.  
On lui applique une tension en entrée (Ue), quelque chose se passe dans le filtre, et on récupère une tension Us à la sortie.

On aimerait caractériser le "bidule" au milieu. Est ce qu'il "augmente" la tension, est ce qu'il la diminue ? Et tout ça sur quelles fréquences ? 

On définit le gain comme étant le rapport de l'amplitude de la sortie sur l'amplitude de l'entrée.

$` G = \frac {U_S}{U_E} `$

- Si le gain est plus grand que 1 : l'entrée est amplifiée : Us > Ue 
- Si le gain est inférieur à 1 : l'entrée est atténuée : Us < Ue
- Si le gain est de 1 : l'amplitude de Us égale celle de Ue

On ne peut pas amplifier sans apporter de l'énergie.

On pourra parler de gain en "grandeur" (tension, intensité) mais aussi en énergie.

_**Question : Est ce qu'amplifier impose la déformation du signal ?**_

_Si on suppose un diagramme de bode avec une courbe plate : toutes les fréquences sont modifiées de la même manière.  Dans ce cas le signal n'est pas déformé._

_Par contre si certaines fréquences sont atténuées plus que d'autres (par exemple), un signal complexe (qui est la somme de sinusoides de fréquences différentes ) n'aura pas la même tête à la sortie si certaines de ses composantes ont été atténuées plus que d'autres._



----

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep010.md) nous parlerons inductances et bobines.









