# EFS ep10 : Inductances et bobines

**Épisode 10 ( 29 juillet 2022 )** 

<img src="images/efs_010/01_chat_bobine.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=0YV06dMrknM
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1546337170

# Previously in EFS
[![youtube](images/youtube.png) (00:01:00)](https://www.youtube.com/watch?v=0YV06dMrknM&t=1m0s)  

## Maxwell (et ses 4 équations)
![](images/efs_010/02_maxwell.png)  

Ces 4 équations jouent sur 2 champs :
- Le champ $`\vec E`$ électrique
  - généré par des charges (équation Maxwell-Gauss). Dès qu'une charge $`\rho`$ est quelque part dans l'espace, il y a un champ radial ou convergent (qui s'éloigne ou se rapproche selon le signe de la charge) de cette charge. 
  - Maxwell-Faraday indique qu'une variation de champ magnétique génère un champ éléctrique.
- Le champ $`\vec B`$ magnétique
  - l'équation de Maxwell-Flux indique qu'il n'existe pas de "charge magnétique".
  - Maxwell-Ampère indique que  pour générer un champ magnétique il faut soit :
    - un terme $`\vec J`$  : un déplacement de charge, autrement dit un courant électrique.
    - un champ électrique variant $`\frac {\partial  \vec E} {\partial t}`$


## Les composants déjà vus

- la résistance, composant linéaire 
  - U=RI
- le condensateur, qui "stocke" de l'énergie en mettant en place un champ $`\vec E`$ entre ses armatures.
  - Sa capacité est notée C
  - L'unité de capacité est le  Farad (F) 
  - L'intensité dans un condensateur n'est pas liée à la tension à ses bornes, mais à la variation de cette tension : $`i(t) = \frac {dU_c}{dt} `$  
  ![](images/efs_010/03_condensateur.png)



# Nouveau composant : la bobine

## Description et symbole
- La bobine est représentée par un symboles de spires. 
- Elle est composée par un fil conducteur enroulé.
- On parle de "self inductance", ou "self" en anglais.
- Sa valeur (son inductance) est notée L
- L'unité de l'indutance est le Henry (H). Comme pour le Farad, c'est une unité gigantesque, on parlera plus souvent de millihenry, microhenry ou nanohenry

À partir du moment ou un courant circule dans la bobine, un champ magnétique ve se mettre en place.  
A l'intérieur des spires, ce champ est parallèle à l'axe de la bobine (dans un sens ou dans l'autre selon le sens d'enroulement et le sens du courant).  La ligne de champ magnétique passe "au travers" de la spire, ce qui fait que le champ généré par le fil enroulé de chaque spire s'aditionne à celui des spires voisines. 

L'équation de maxwell-Flux indique que le champ magnétique $`\vec B`$ (en rouge ci-dessous) va "boucler", tout ce qui "rentre" d'un coté devant "ressortir" de l'autre, la divergence du champ étant nulle.

![](images/efs_010/04_bobine_champ.png)

## Accumulation d'énergie
Le champ magnétique, comme le champ électrique, est capable d'accumuler de l'énergie, mais le principe est différent. 

Une analogie peut être faite avec une bassine remplie d'eau, dans laquelle on essaie, avec la main, de faire tournoyer l'eau.  
Au début on sent une résistance de l'eau immobile qui s'oppose au mouvement de la main. Puis petit à petit, l'eau va se mettre à tournoyer, et la main ne sentira plus de résistance, elle entretiendra juste le mouvement en "suivant" l'eau. De par son mouvement, l'eau a accumulé de l'énergie.  
Si ensuite on cherche à freiner l'eau en maintenant la main fixe dans la bassine, le mouvement de l'eau poussera sur la main, l'eau résistant alors au chngament imposé (le fait de vouloir la stopper). 

Il en va de même pour le courant électrique (la main) et le champ magnétique dans la bobine (l'eau).  
La bobine va s'opposer à tout changement du courant (que ce soit à la mise en place d'un courant à l'allumage du circuit, ou à l'arrêt du courant à l'extinction du circuit). 


## Ça ressemble à quoi ?

![](images/efs_010/05_bobines.png)
![](images/efs_010/06_bobine2.png)
![](images/efs_010/07_bobine3.png)
![](images/efs_010/08_bobine_ouverte.png)f

c'est un simple fil fin (enduit d'une couche de vernis isolant) entouré autour d'un mandrin isolant.

## la bobine reste un fil
La bobine reste un fil, donc quand on lui applique un courant continue, en dehors du moment ou on applique le courant et le moment ou on le coupe (la bobine resiste alors au changement de courant), on met le générateur en court-circuit !  


## Équation temporelle courant/tension 

$`U_L(t) = L \frac {di}{dt}`$  

La tension au borne de la bobine dépend de sa valeur d'inductance, et de la variation d'intensité passant au travers.


On se rapelle que pour le condensateur, c'est l'intensité qui dépend de la variation de tension à ses bornes (et de sa capacité).


### Intensité constante

Si l'intensité est constante, $`\frac {di}{dt} = 0`$ donc $`U_L=0`$ :  Si l'intensité est constante, la tension au borne de la bobine est de 0V : elle se comporte comme un simple fil conducteur.

### Augmentation du courant
Quand on amène du courant dans la bobine, (passage de 0 à quelques mA) $`\frac {di}{dt} > 0`$

$`U_L > 0`$ : la bobine "résiste" au passage du courant


### Diminution du courant
Quand on enlève le courant dans la bobine, (passage de quelques mA à 0) $`\frac {di}{dt} < 0`$

$`U_L < 0`$  Une tension inverse se crée aux bornes de la bobine, qui de nouveau "résiste" à un courant qui est en train de se retirer.


## Montages

Montons un générateur à une bobine en série avec une résistance.
- $`U_G`$ est la tension aux bornes du générateur
- $`U_R`$ est la tension aux bornes de la résistance
- $`U_L`$ est la tension aux bornes de la bobine


![](images/efs_010/09_montage.png)  

### Réponse temporelle à un signal carré
[![youtube](images/youtube.png) (00:37:41)](https://www.youtube.com/watch?v=0YV06dMrknM&t=37m41s)



Si le générateur est réglé pour envoyer un signal carré:  
Quand on branche le montage, l'oscillo nous montre :
- $`U_G`$ en jaune
- $`U_L`$ en bleu

![](images/efs_010/10_oscillo_bobine.png)

A chaque "front" de la tension générateur, on voit que la tension au borne de la bobine "pique" brusquement (en positif, quand Ug augmente, en négatif quand Ug diminue) avant de redescendre à zero et d'y rester quand Ug ne bouge plus (que ce soit à l'état haut ou à l'état bas)

### Réponse tension et intensité à un signal sinusoïdal

- Une voie de l'oscillo récupère $`U_R`$, donc l'intensité a un facteur prêt (en jaune)
- L'autre voie mesure $`U_L`$ (en bleu)

_Bien penser à utiliser une sonde différentielle sur $`U_R`$ car les bornes (-) des voies de l'oscillo sont reliées à la terre.  
Attention, brancher le commun des voies de l'oscillo au niveau de $`U_R`$ et mesurer $`-U_L`$ ne fonctionne pas, car les (-) de l'oscillos sont reliées entre elles mais aussi à la terre. le générateur ayant sont (-) relié a la terre aussi, ça va poser problème._

Le résultat à l'oscilloscope montre un déphasage de $`\frac \pi 2 `$ entre le courant et la tension. Cette fois, contrairement au condenstateur, le courant est en retard sur la tension.

![](images/efs_010/11_oscillo_sinus.png)


Si on fait varier la fréquence du générateur, la tension $`U_L`$ voit son amplitude variée : plus la fréquence augmente, plus $`U_L`$ augmente.  
Le déphasage entre courant et tension, lui, ne varie pas.


## Étude fréquentielle
[![youtube](images/youtube.png) (01:10:13)](https://www.youtube.com/watch?v=0YV06dMrknM&t=1h10m13s)   

On a vu plus haut l'équation temporelle de l'inductance :
$`U_L(t) = L \frac {di}{dt}`$ 

On cherche i pour une tension sinusoïdale $`U_L(t) = A \_cos (\omega t + \phi)`$

$`\frac {di}{dt}  = \frac 1 L U_L(t) `$ 

Pour trouver i, on doit utiliser une intégrale

$`i(t) = \frac 1 L \int U(t) dt \\
i(t) = \frac 1 L \frac 1 \omega  A \sin (\omega t + \phi) \\
i(t) = \frac 1 {L \omega}  A \cos (\omega t + \phi - \frac \pi 2)
`$

On peut maintenant passer au représentations complexes :

$`\underline U_L(t) = [A, \phi] = Ae^{j\phi}  \\
\underline i(t) = [\frac A {L \omega} , \phi -  \frac \pi 2]
`$ 

donc comme l'impédance complexe est :
$`
\underline Z = \frac {\underline U}{ \underline i} \\
\underline Z =  \frac {[A, \phi]} {[\frac A {L \omega} , \phi -  \frac \pi 2]}
`$

Pour diviser 2 nombres complexes, on divise leurs modules, et on soustrait leurs arguments

$`
\underline Z =  [\frac A {\frac A {L \omega}} , \phi - (\phi -  \frac \pi 2)] \\
\Big\| \ \ \   \underline Z =  [L \omega ,  \frac \pi 2] 
`$

Dans le plan complexe, le déphassage de $`\frac \pi 2`$ impose l'impédance sur l'axe des  imaginaires positifs, à  une distance de $`L\omega`$ 

On obtient donc une impédance de l'inductance 
$`\underline Z_L = jL\omega`$

![](images/efs_010/12_impedance_bobine_plan_imaginaire.png)


# Récapitulatif sur les 3 composants parfais R, C et L
[![youtube](images/youtube.png) (01:22:29)](https://www.youtube.com/watch?v=0YV06dMrknM&t=1h22m29s) 

Impédance des trois composants étudiés :

$`
\underline i = \frac {\underline U}{ \underline Z}
\\ \ \\
\left\{
    \begin{array}{ll}
  \underline Z_R = R \\
  \underline Z_C = \frac 1 {jC\omega} =- \frac j {C\omega}  \\
  \underline Z_L = jL\omega
  \end{array}
\right.
`$

pour $`\omega = 0`$ :
- $`Z_R=R`$
- $`Z_C \rightarrow \infty  \Rightarrow`$ circuit ouvert
- $`Z_L \rightarrow 0 \Rightarrow`$ conducteur

pour $`\omega \rightarrow \infty`$ :
- $`Z_R=R`$
- $`Z_C \rightarrow 0   \Rightarrow`$ conducteur
- $`Z_L \rightarrow \infty  \Rightarrow`$ circuit ouvert


# Pour des composants réels
[![youtube](images/youtube.png) (01:30:04)](https://www.youtube.com/watch?v=0YV06dMrknM&t=1h30m04s) 

Avec les formules d'impédances complexes déterminées pour des R, L et C parfaits, on va pouvoir commencer à tenir comptes du fait que les composants "réels" ont des défauts.

Par exemple : 
- une bobine possède une résistance. 
- Un condensateur à une résistance de fuite qui le décharge spontanément, mais aussi une résistance série (ESR). De plus ses pattes sont des petites inductances série (ESL).  
  ![](images/efs_010/13_modele_condo_reel.png)
   
On peut ainsi créer des modèles équivalents de composants réels, mais on ne le fait que si on en a besoin. Par exemple l'inductance série et la résistance parallèle d'un condenstaeur peuvent être néglligées à des fréquences "raisonables".

### Pistes de CI
Les pistes des circuits imprimés elles mêmes ne sont pas parfaites. Ce sont de petites inductances. La présence d'un plan de masse de l'autre coté du PCB amène aussi une capacité parasite. Une piste réelle pourra donc être schématisée, au besoin, comme suit :  
![](images/efs_010/14_equivalence_piste.png)

### Cables et lignes de transmission
De la même façon un cable coaxial, si on en coupe un morceau, possède : 
- une résistance (conducteur non parfait)
- une inductance (conducteur d'une certaine longueur dans lequel passe du courant)
- une capacité (le fil et le blindage sont des conducteurs séparés par un isolant)
Par construction ces cables ont une impédance connue (50Ω par exemple) dont il faut tenir compte pour ne pas dégrader le signal qu'ils conduisent.

### Modèle "universel"
Même si rarement utilisé en pratique car chaque cas à ses propres simplifications, un modèle général permettant de caractériser tous les composants peut être :  
![](images/efs_010/15_modele_complet.png)


# Conclusion : cramage de bobine
[![youtube](images/youtube.png) (01:52:26)](https://www.youtube.com/watch?v=0YV06dMrknM&t=1h52m26s)  


![](images/failed_bowling.webp)

La solution pour cramer une self serait de la connecter à un générateur continu, en court-circuit. Cependant, magré les différents essais, les bobines chauffent mais résistent...  
Elles restent même fonctionnelles après la torture.

![](images/failed.png)



----

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep011.md) nous continuerons à parler des filtres (RC et RL).








