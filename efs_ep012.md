# EFS ep12 : RC et RLC

**Épisode 12 ( 26 août 2022 )** 

<img src="images/efs_012/01_chat_noir.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=nrTF_H4Anf0
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1573173104

# Previously in EFS
[![youtube](images/youtube.png) (00:02:22)](https://www.youtube.com/watch?v=nrTF_H4Anf0&t=2m22s) 

Concernant les 3 composants de bases :
- la résistance R
- le condensateur C
- l'inductance L
 
## Comportement fréquentiel
Les 3 impédances complexes à connaitre :

$`
\underline i = \frac {\underline U}{ \underline Z}
\\ \ \\
\left\{
    \begin{array}{ll}
  \underline Z_R = R \\
  \underline Z_C = \frac 1 {jC\omega} =- \frac j {C\omega}  \\
  \underline Z_L = jL\omega
  \end{array}
\right.
`$

- Pour les basses fréquences ($`\omega \rightarrow 0`$) :
  - $`Z_R=R`$
  - $`Z_C \rightarrow \infty  \Rightarrow`$ circuit ouvert
  - $`Z_L \rightarrow 0 \Rightarrow`$ conducteur

- Pour les hautes fréquences ($`\omega \rightarrow \infty`$) :
  - $`Z_R=R`$
  - $`Z_C \rightarrow 0   \Rightarrow`$ conducteur
  - $`Z_L \rightarrow \infty  \Rightarrow`$ circuit ouvert

## Comportement temporel

### Résistance
$`U_R(t) = R.i_R(t)`$

### Condensateur
```math
q(t)=C.U_C(t) \\

\frac {dq} {dt}=C \frac {dU_C} {dt}

 \underbrace{ \frac {dq} {dt}}_{i(t)} =C\frac {dU_C} {dt}
 ```


### Inductance
$`U_L(t) = L \frac {di}{dt}`$


## Cekelonsé
L'inductance et la capacité sont antagoniste :
- le condensateur travaille avec un champ électrique, la bobine avec un champ magnétique.
- Le courant dans un condensateur est lié à la variation, par rapport au temps, de la tension à ses bornes. La tension aux bornes d'une inductance est liée à la variation, par rapport au temps, du courant la traversant.


Qu'on utilise l'analyse en version temporelle ou en version fréquentielle, on obtiendra toujours le même résultat.  
Cependant l'un ou l'autre type d'analyse sera plus facile à utiliser selon le problème à étudier.  
Par exemple, dans le cas d'une étude de la charge/décharge d'un condensateur, l'analyse temporelle sera plus adaptée.  
Si par contre on veut savoir comment un circuit RC réagit en fonction d'un signal, on pourra découper le signal en sinusoides "simples" et étudier fréquentiellement le filtre.

Exemple pour un condensateur "de découplage" sur une  entrée d'un processeur :  
![fin](images/efs_012/02_condo_decouplage.png)
  - l'analyse "temporelle" voit le condensateur comme une réserve d'énergie qui se "décharge" légèrement lors d'un fort appel de courant du microprocesseur. Le condensateur "compense" des glitchs de l'alimentation ou des besoins "brusques" du processeur.
  - l'analyse "fréquentielle" voit le condensateur comme un filtre passe bas qui atténue les hautes fréquences (sans toucher aux basses fréquences). En effet, un appel brusque de courant est en fait un pic HF vers le bas de la ligne d'alimentation. Ce filtre atténue la dynamique du "pic" en coupant ses composantes HF.



## Fonction de transfert du filtre passe bas RC

Le filtre passe bas RC vu déja plusieurs fois, est assimilable à un pont diviseur auquel on peut appliquer la loi d'ohm complexe, sur les impédances.
 
![](images/efs_012/03_passe_bas_rc.png)

Les impédances des 2 éléments du pont sont :
  - $`\underline{Z_R} = R `$
  - $`\underline{Z_C} = \frac 1 {jx\omega}  `$

La formule "complexe" du pont diviseur est :

```math
\underline{U_S}= \frac {\underline{Z_c}} {\underline{Z_R} + \underline{Z_C}} \underline{U_{E}} 
\\ \  \\
\frac {\underline{U_S}} {\underline{U_{E}}} = \frac {\frac 1 {jC\omega} } {R + \frac 1 {jC\omega}}  
\\ \  \\
\frac {\underline{U_S}} {\underline{U_{E}}} = \frac {1} {RjC\omega +1 }  
``` 

Ici on a déterminé la **fonction de transfert** du filtre : la relation qui permet, connaissant l'entrée du filtre, d'en calculer la sortie : $`\frac {\underline{U_S}} {\underline{U_{E}}} = \frac {1} {RjC\omega +1 }  `$

C'est un nombre complexe, donc si on veut calculer : 
- le rapport entre l'entrée et la sortie ⇒ on va calculer le module de ce nombre complexe
- le déphasage entre l'entrée et la sortie ⇒ on va calculer l'argument de ce nombre complexe (angle en radian)


## Diagramme de bode : échelle logarithmique et décibels

Le diagramme de Bode se compose de 2 graphiques ou on souhaite représenter en ordonnée :  
- le gain $` | \frac {U_S} {U_E} |_{dB}`$ (en décibel)
- et la phase $`\arg (\frac {U_S}{U_E} )`$ (en rad) 
Pour toute une gamme de fréquence (ou pulsation) en abscisse

### Échelle logarithmique pour les fréquences (ou pulsations) en abscisse
La gamme de fréquences en abcisse pouvant être très importante (de quelques Hertz à plusieurs centaines de giga Hertz), on ne peut pas utiliser une échelle linéaire en gardant une certaine précision dans chaque gamme de fréquence.  
On triche donc en condensant les choses : entre chaque graduation des abscisses, on multiplie par 10 l'échelle. C'est une échelle logarithmique

### Décibel pour le gain en ordonnée
Le gain peut lui aussi prendre des valeurs très faibles ou très grandes. Pour garder une échelle linéaire (comme pour la phase qui ne varie qu'entre $`-\pi`$ et $`\pi`$), on va "compresser" les grandes valeurs de gain en utilisant les décibels :  
$` | \frac {U_S} {U_E} |_{dB} = 20 \log (\frac {U_S}{U_E})`$

**Attention** : la définition des décibels est faite pour un rapport **d'énergie** $`(\frac {E_S} {E_E})_{dB} = 10 \log \frac {E_S} {E_E}`$  
Quand on utilise un rapport de grandeurs de champs (tension, intensité...), puisque  $`puissance = \frac {énergie} t = \frac {U^2} R`$, un rapport d'énergie est est un rapport de carré (U² ou I²) . Le carré peut sortir du log (car $`\log(x^n) = n \log (x)`$) et on retrouve bien 2 x 10 log (Us/Ue) pour les décibels de grandeurs de champs. 


Quand le gain en décibel est :
- positif : le signal est amplifié 
- négatif : le signal est atténué (quand ω tend vers ∞ pour le filtre passe bas RC)
- égal à 0 : le signal garde son amplitude (quand ω tend vers 0 pour le filtre passe bas RC)

Le niveau à -3dB est particulièrement intéressant : c'est le niveau ou l'énergie est divisée par 2 (en terme de tesnion, ça correspond à multiplier Ue par $`\frac 1 {\sqrt 2}`$ = 0.707 la tension d'entrée ).  
On pose par convention que quand on diminue l'energie par 2, le signal est "coupé". La fréquence de coupure d'un filtre sera donc la fréquence à laquelle le signal sera atténué de -3 dB.

Après la fréquence de coupure la courbe du gain plonge à -20dB par décade par une droite (sur un graph logarithmique). 


*__Question sur le gain et le déphasage__*  
_[![youtube](images/youtube.png) (00:32:12)](https://www.youtube.com/watch?v=nrTF_H4Anf0&t=32m12s)_  
*La valeur du  gain ne dépend pas du déphasage : le module de Ue/Ue reprend juste le rapport des amplitudes du signal entre l'entrée et la sortie, même pour un signal dont la sortie est déphasée de $`\pi`$ (inversion de phase). Ce signal serait pourtant complètement annulé si on additionnait l'entrée à la sortie (avec un module de 1 ou 0 dB).  
Dans le cas des asservissements ou on a des boucles de rétroaction (on boucle la sortie sur l'entrée), on devra regarder le gain ET la phase pour savoir si la boucle de rétroaction sera stable ou non.*

![](images/efs_012/04_bode_manuel.png)

![](images/efs_012/05_bode.png)



# Fréquence de coupure et pourquoi -20dB /décades ?
[![youtube](images/youtube.png) (00:39:17)](https://www.youtube.com/watch?v=nrTF_H4Anf0&t=39m17s)

## Calcul de la fréquence de coupure

La pulsation de coupure est définie pour $`\frac {U_S}{U_E} = \frac 1 {\sqrt 2}`$

$`
\frac {U_S} {U_E} = \| \frac {1}{RjC\omega +1 } \|
`$

Les mathématiciens nous disent que :  
$`\| a+bj \| = \sqrt {a^2 + b^2}`$  
 le module est la distance en rouge ci-dessous :  
 ![fin](images/efs_012/06_module.png)  
 De plus le module d'un quotient est le quotient des modules  

$`
\frac {U_S} {U_E} = \frac {1}{\sqrt {1 + \underbrace {R^2 C^2 \omega^2}_{=1}}} `$

 Si on cherche $`\frac {U_S}{U_E} = \frac 1 {\sqrt 2}`$ on voit qu'on à juste à trouver la valeur de $`\omega_c`$ tel que $`R^2 C^2 \omega_c ^2 =1`$   
 donc $`\omega_c= \frac 1 {RC} `$    ou  $` f_c = \frac 1 {2\pi RC}`$


## -20dB/décades
le gain (pas encore en décibels) calculé plus haut est :  
$` \frac {U_S} {U_E} = \frac {1}{\sqrt {1 + R^2 C^2 \omega^2}} `$  

En décibel :
$` \frac {U_S} {U_E})_{dB} = 20 \log (\frac {1}{\sqrt {1 + R^2 C^2 \omega^2}}) `$   

Comment se comporte cette relation dans les hautes fréquences  (grandes valeurs de $`\omega`$) ?   
Dans ce cas le "1+"est négligeable devant un $`\omega ^2 tres grand`$
On peut approximer $` \frac {U_S} {U_E})_{dB} \approx 20 \log (\frac {1}{\sqrt {R^2 C^2 \omega^2}}) \approx 20 \log (\frac {1}{R C \omega}) \approx -20 \log (R C \omega)`$    

- Pour $`\omega \rightarrow(\frac {U_S} {U_E})_{dB} \approx -20 \log (R C \omega)`$
- Pour $`10 \omega \rightarrow(\frac {U_S} {U_E})_{dB} \approx -20 \log (10 R C \omega) \approx -20 \log (R C \omega) - 20 \log (10) \approx -20 \log (R C \omega) - 20 `$   (car log(10)=1)  
   
⇒ Quand on multiplie la fréquence par 10, on obtient le "gain  moins 20" ... d'où les -20 dB par décades en asymptotes pour les hautes fréquences d'une filtre de premier ordre passe bas.

![](images/efs_012/07_coupure_20db_decade.png)


# Les filtres RL 
[![youtube](images/youtube.png) (01:05:23)](https://www.youtube.com/watch?v=nrTF_H4Anf0&t=1h5m23s) 

Il est possible de fabriquer des filtres RL comme les filtres RC. On a tendance à moins parler des filtres RL car les bobines sont moins simples à fabriquer/utiliser que les condensateurs : la bobine est volumineuse, et génère des interférances electro magnétiques pour les autres composants.  
En pratique les bobines sont souvent là quand il y a des transformateurs ou de la filtration d'alimentation. Les filtres RC sont bien souvent plus utilisés que les RL.

![](images/efs_012/08_RL.png)

La fréquence de coupure d'un filtre RL est $`\omega_C = \frac R L `$  ou $`f_C = \frac R {2\pi L} `$



# Les filtres RLC 
[![youtube](images/youtube.png) (01:13:03)](https://www.youtube.com/watch?v=nrTF_H4Anf0&t=1h13m03s) 

![](images/efs_012/09_rlc_serie_parallele.png)

Le filtre RLC série voit une résistance, une bobine et un condesateur mis en série, on regarde Us : la sortie aux bornes du condensateur.

Le filtre RLC parallèle voir une résistance, une bobine et un condensateur montés en parallèle de l'alimentation.

Les circuits RLC sont des circuits oscillants qui vont avoir un comportement particuliers pour certaines fréquences.  En effet, le condesnateeur et la bobine ayant des comportements "antagonistes" ils vont pouvoir se "renvoyer la balle" de leurs stockages d'énergie respectifs.

## Équation différentielle

Pour le circuit RC (analyse temporelle déja vue) :  
```math 
U_R = R i(t)  \\ \ \\


i(t)= C \frac {dU_C} {dt} \\ \ \\

E=U_R(t) +U_C(t)  \\\ \\
   E = Ri(t) +U_C(t)  \\\ \\
  E = RC \frac {dU_C} {dt}  + U_C(t)
```


Pour les circuit RLC on va passer au équations différentielles du second ordre (faisant intervenir des dérivées secondes).  
Par exemple pour un circuit RLC série :

![](images/efs_012/10_rlc_serie_etude.png)

$` V = U_R + U_L + U_C `$

$` V = Ri(t) + L \frac {di(t)} {dt} + \frac {1} {C} \int i(t) dt `$


Dérivons tout ça :  
$` 0 = R \frac {di} {dt} + L \frac {d^2i} {dt} + \frac {1} {C} i(t) `$

On divise par L pour avoir la dérivée seconde sans facteur :  
$` \frac {d^2i}{dt} + \frac {R} {L} \frac {di} {dt} +  \frac {1} {LC} i(t) = 0 `$

- $`\frac {d^2i}{dt} `$ et $`  \frac {1} {LC} i(t) `$ sont des termes amenant une oscillation ( la bobine et le condensateur se renvoient de l'énergie)
- $`\frac {R} {L} \frac {di}{dt}`$ est un terme d'amortissement (R dissipe de l'énergie par effet joule)

Sans le terme en R/L on aurait une oscillation parfaite. Avec la résistance, il y a atténuation de l'oscillation au court du temps.  
L'amortissement peut être plus ou moins rapide selon le terme d'amortissement :
- amortissement fort : courbe rouge ci-dessous
- amortissement plus faible : couurbe verte du bas
- pas d'amortissement : oscillation du haut

![](images/efs_012/../efs_011/../efs_012/11_oscillation_amortissement.png)



----
![fin](images/thats_all_folks_bugs.png)

Le [prochain épisode](efs_ep013.md) approfondira les circuit RLC et leurs oscillations, notamment leurs fréquences  et valeurs particulières.











