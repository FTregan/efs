# EFS ep13 : Circuits LC et RLC Stay tuned !

**Épisode 13 ( 02 septembre 2022 )** 

<img src="images/efs_013/01_chat_cri.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=0HfksZvxGiU
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1579765436

# Previously in EFS
[![youtube](images/youtube.png) (00:01:39)](https://www.youtube.com/watch?v=0HfksZvxGiU&t=1m39s) 

## Retour sur les décibels
[![youtube](images/youtube.png) (00:03:42)](https://www.youtube.com/watch?v=0HfksZvxGiU&t=3m42s) 

Pour avoir des ordres de grandeurs en décibels, utilisons un tableur avec quelques "traductions" avec une amplitude de **10V** en entrée.

dB    | Ampl. sortie (V)
---: | ---:
0     | 10
-3    | 7.07948
-20   | 1
-40   | 0.1 
-60   | 0.01
-80   | 0.001

- 0 dB = pas d'atténuation (on reste à 10V en sortie), 
- à chaque 20dB de moins, on divise l'amplitude du signal par 10

Rappel : en radio, on parle souvent en dBm, qui est un dB avec une source théorique de 1 mW


## circuit RLC

2 circuits principaux : RLC série et RLC parallèle.   

### Pour le circuit série :

On a vu que 
```math 
\begin{align*}
&E = U_R + U_L + U_C
\\ \ \\
donc\  que\ \ &E = Ri(t) + L \frac {di(t)} {dt} + \frac 1 C \int i(t) dt 
\\ \ \\
Or\ \ \  &i(t) = C \frac {dU_C} {dt}
\\ \ \\
donc \ \ &E = LC \frac {d^2U_C} {dt^2} + RC \frac {dU_C}{dt} + U_C 
\end{align*}
```

C'est le comportement en temps du circuit série. C'est ce qu'on verrait en alimentant E puis en enlevant l'alim et en reliant les 2 fils ensemble pour fermer le circuit.
- Les termes $`LC \frac {d^2U_C} {dt^2}  + U_C `$ est l'équation "harmonique", dont les solutions sont des fonctions périodiques
- Le terme de premier ordre $`RC \frac {dU_C}{dt}`$ traduit un amortissement.

### pour le circuit parallèle
On ne peut pas étudier le circut de la meme façon parce que si on mesure Uc, on mesure E

Commençons par étudier les circuits sans amortissement :


# Étude LC (sans amortissement)

## LC série

l'impédance du circuit :
```math
\begin{align*}
Z &= \underbrace {Z_L}_{j\omega L} + \underbrace {Z_C}_{\frac 1 {jC\omega}}
\\ \ \\
Z &= j\omega L + \frac 1 {jC\omega}
\\ \ \\
Z &= \frac {j^2\omega^2 L  C + 1} {jC\omega}
\\ \ \\
Z &= \frac {-\omega^2 L  C + 1} {jC\omega}
\\ \ \\
Z &= - \frac {-j\omega^2 L  C + j} {C\omega}
\\ \ \\
\Big\|\ \ \ \ \ \ Z &= \frac {j(L  C \omega^2 - 1)} {C\omega}
\\ \ \\
\end{align*}
```

On voit une valeur particulière dans la parenthèse : il est possible d'annuler l'impédance du circuit si  :  
**LCω²=1**

### A retenir pour le circuit LC série

Pulsation critique (ou pulsation propre) ω₀ :  
$`\omega_0 = \frac 1 {\sqrt{LC}}`$

Le circuit a :
- Z≠0 pour ω ≠ ω₀ 
- Z=0 pour ω = ω₀ : c'est un circuit passe bande.

## LC parallèle

l'impédance du circuit :
```math
\begin{align*}
\frac 1 Z &= \frac 1 {Z_C} + \frac 1 {Z_L}
\\ \ \\
\frac 1 Z &= jC\omega  + \frac 1 {jL\omega}
\\ \ \\
\frac 1 Z &= \frac {j^2LC \omega^2 + 1} {jL\omega}
\\ \ \\
Z &= \frac {jL \omega} {j^2LC\omega^2  + 1}
\\ \ \\
\Big \| \ \ \ \ \  Z &= \frac {jL \omega} {1 - LC\omega^2 }

\end{align*}
```

On voit aussi un point particulière dans la parenthèse : il est possible d'avoir une impédance infinie du circuit (division par 0) si  :  
**LCω²=1**

### A retenir pour le circuit LC parallèle

Pulsation critique (ou pulsation propre) ω₀ :  
$`\omega_0 = \frac 1 {\sqrt{LC}}`$

Le circuit a :
- Z ≠ 0 pour ω ≠ ω₀ 
- Z → ∞ pour ω = ω₀ : c'est un circuit rejecteur de bande.


## Circuits LC associés dans des filtres.
C'est l'association dans des filtres qui va permettre d'utiliser ces circuits en passe bande ou coupe de bande.

### exemple avec un filtre lc parallèle
![](images/efs_013/02_filtre_avec_lc_parallele.png)

- pour ω = ω₀ l'impédance du LC est infinie, il se comporte comme un circuit ouvert, le signa passe.
- pour ω ≠ ω₀ , l'impédance de LC est "finie", donc on a un pont diviseur, le signal subit une atténuation

On a un filtre passe bande.  

Ce n'est pas habituel d'utiliser ce genre de circuit, mais ça fonctionne.

### exemples avec un circuit LC série
![](images/efs_013/03_filtre_avec_lc_serie.png)

#### Résistance en "entrée" :
- Pour ω = ω₀, l'impédance du LC est nulle, donc il court-circuite tout à la masse. 
- Pour ω ≠ ω₀, l'impédance de LC est non nulle et on a une atténuation du signal
  
On a un filtre coupe-bande.

#### résistance en "sortie" :
- Pour ω = ω₀, l'impédance du LC est nulle, et se comporte comme un conducteur : Ur=UE 
- ω ≠ ω₀ : l'impédance de LC est non nulle et atténue le signal

On a un filtre passe bande


### Utilisations "classiques" des RLC

les LC parallèle vont être utilisés en radio pour "tuner" des fréquences (avoir un passe bande autour de la fréquence à laquelle on s'interesse.)

Les RLC série se voient en modélisation de signaux de longues lignes

Les bobines sont aussi utilisées en débruitage.

On n'utilise des bobines dans les filtres que si on en a besoin car c'est gros et onéreux. Par contre par rapport à un condensateur ça vieillit plutôt bien.


# En vrai à l'oscilloscope

## R - LC série

![](images/efs_013/04_circuit1_oscilllo.png)

pour ce circuit on a :
- un condensateur de 1µF
- une bobine de 10mH
- une résistance (peut importe la valeur)

donc  
- ω₀ = 10⁴ rad
- f₀ = 10⁴ x 2π = 1592 Hz

 On alimente le circuit avec une sinusoïde dont on fera varier manuellement la fréquence.   
 On mesure la tension LC à l'oscillo.

- à 10Hz  
   ![](images/efs_013/05_amplitude_a_10hz.png)
- avant 400Hz, l'amplitude ne bouge pas sur l'oscillo et est maximum.
- à 400HZ, on distingue un léger recul de l'amplitude (de la largeur du trait de l'oscillo)
- ensuite jusqu'a 1600Hz, on voit l'amplitude diminuer jusqu'a un minimum (en vrai on n'a pas 0 car les composants ne sont pas parfait)
- à 1600Hz  
  ![](images/efs_013/06_amplitude_a_1600Hz.png)
- au dessus de 1600Hz, l'amplitude remonte jusqu'a retrouver son maximum.
  

## R - LC parallèle
pour ce circuit on a toujours :
- un condensateur de 1µF
- une bobine de 10mH
- une résistance (peut importe la valeur)

donc  
- ω₀ = 10⁴ rad
- f₀ = 10⁴ x 2π = 1592 Hz


En faisant varier la fréquence de l'entrée, on voit que le signal est a sa pleine amplitude pour ω = ω₀
- à 100Hz, amplitude faible :  
  ![](images/efs_013/07_lc_parallele_amplitude_100hz.png)
- à 1600Hz, amplitude max :   
  ![](images/efs_013/08_lc_parallele_amplitude_1600hz.png)
- Quand on augmente encore au dela de 1600Hz, l'amplitude se remet a diminuer.


# Apparté : questions et informations diverses
Pour afficher automatiquement un diagramme de Bode, un oscillo ne suffit pas (il faut un générateur qui balaie les fréquences variables).  
- https://www.youtube.com/watch?v=uMH2hGvqhlE
  
Il existe des montages/circuits permettant ça : 
- wobubulateurs (?)
- VNA (nanovna) : https://connect.ed-diamond.com/Hackable/hk-036/introduction-a-l-analyseur-de-reseau-le-nanovna-pour-la-caracterisation-spectrale-de-dispositifs-radiofrequences



Petit cours intéressant sur les filtres et comment les cascader.  
- source : http://compilonet.free.fr/ptsi/Cours%20de%20physique/Relec6.pdf
- [fichier pdf récupéré](images/efs_013/Relec6.pdf)


Site a ne pas consulter si on veut garder les restrictions logiciel de son oscillos rigol. (Réalisé par des professionnels, ne faites pas ça chez vous !) : https://gotroot.ca/rigol/riglol/



# Cas concret 
## Récepteur radio FM
source : https://www.electroschematics.com/tiny-fm-radio/

![](images/efs_013/09_recepteur_fm.png)

On s'interesse uniquement au filtre RC relié a l'antenne, qui est censé filtré la longueur d'onde a écouter.

ici la bobine est "décrite", on ne nous donne pas sa valeur.  
Un [calculateur](https://www.allaboutcircuits.com/tools/coil-inductance-calculator/) permet de cacluler une valeur en Henry connaissant : 
- le diametre des tours (surface du cœur)
- le nb de tours 
- la longueur du solénoïde 
- le matériau du cœur (ici de l'air).
  
On obtient quelques chose aux alentours de 0.25 µH. avec 22pF pour le condensateur :
f₀= 67MHz. 

C'est un peu en dessous de la bande FM broadcast, mais les ordres de grandeurs des fréquences sont bonnes.


## Récepteur AM

![](images/efs_013/10_recepteur_am.png)

On calcul f₀ = 500kHz

bande AM : 526.5 Khz - 1606.5kHz

On est dans les bons ordres de grandeurs.


# Pour aller plus loin

Pour le moment, les filtres qu'on a vu ne sont pas tres "plats" sur leur partie passante (filtres simples avec composants imparfait)

Les [filtres de Butterworth ![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Filtre_de_Butterworth) ont des caractéristiques plus plates et peuvent être cascadés pour obtenir des ordres supérieurs. (plus l'ordre est grand plus la pente d'atténuation est forte)

Il existe aussi les [Filtre_de_Tchebychev ![wikipedia](images/wikipedia.png) ](https://fr.wikipedia.org/wiki/Filtre_de_Tchebychev)

On étudiera scertainement ça plus tard.



----
![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep014.md) nous passerons aux semi-conducteurs : les diodes !










