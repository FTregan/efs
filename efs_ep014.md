# EFS ep14 : Semi-conducteurs et diodes

**Épisode 14 ( 09 septembre 2022 )** 

<img src="images/efs_014/01_chat_tete_cote.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=tCFy16qBfMw
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1586258415

# Introduction
## préambule 
[![youtube](images/youtube.png) (00:00:43)](https://www.youtube.com/watch?v=tCFy16qBfMw&t=0m43s) 

Changement de thème aujourd'hui.  
Après avoir passé quelques temps sur les composants linéaires : les résistances, condensateurs et inductances, nous continuons notre voyage  en remontant dans le temps pour se rapprocher petit à petit du présent et des semi-conducteurs modernes.


## La galène, l'ancêtre de la diode
[![youtube](images/youtube.png) (00:02:56)](https://www.youtube.com/watch?v=tCFy16qBfMw&t=2m56s) 

![](images/efs_014/02_galene.png )

La galène [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Galène) est un sulfure de plomb cristallin; que l'on trouve à l'état naturel et qui est connu depuis l'antiquité.  

en 1874 Karl Braun découvre qu'en appuyant une aiguille métallique sur un morceau de galène, et en appliquant une tension à ce montage, le courant électrique passe dans un sens mais pas dans l'autre.  
![](images/efs_014/03_galene_diode.png)  
Cette découverte lui vaudra  un prix Nobel, partagé avec Marconi.

La galène a permis de construire les premiers récepteurs radio.  
Les postes à galène étaient des radios sans source d'énergie (autre que le signal radio capté par l'antenne). 

![](images/efs_014/04_sch%C3%A9ma_poste_galene.png)

Sur ce schéma de poste radio (AM = modulation d'amplitude) très simple, on distingue : 
- l'antenne qui capte le rayonnement électromagnétique ambiant (toutes les radios)
- un filtre LC qui va permettre de sélectionner la fréquence a écouter
- une diode (le morceaux de galène et son aiguille) qui ne va laisser passer que les amplitudes positives du signal
- d'un casque qui va permettre de filtrer la porteuse et d'écouter le signal audio modulé.  


On remarque que le symbole de la diode ressemble à une aiguille sur un caillou... coïncidence ?


# La diode
Idéalement on souhaiterait un composant qui laisse passer le courant quand la tension à ses bornes est positive, et bloque totalement le courant quand la tension à ses bornes est négative.

![](images/efs_014/05_%20diagramme_u_i_diode_ideale.png)

## Un peu de microélectronique : la jonction PN

![](images/efs_014/06_pn.png)


### Atome et couche de valence
Un atome est composé d'un noyau et d'électrons gravitant autour. Le noyau a une charge positive due aux protons le contenant, les électrons sont chargés négativement.
Chaque élément du tableau périodique a un nombre de protons défini (Carbone : 6,  Silicium : 14, Phosphore 15...), et donc un nombre d'électrons équivalent (les éléments sont neutres électriquement).

Une modélisation classique des électrons autour du noyau, est de les voir empilés en "couches", de plus en plus éloignées du noyau.  
Les électrons remplissent les couches les plus proches du noyau en premier (première couche : 2 électrons max, deuxième couche : 8 électrons max, 3ème couche : 18 électrons max, nième couche : 2n² électrons max...) et le nombre d'électrons de la dernière couche est ce qui va être important pour la suite.

La dernière couche occupée par des électrons, dans un atome, est nommée couche de valence. Les électrons de cette couche sont nommés les électrons de valence.  

Les électrons de valence d'un atome aiment bien former des paires avec ceux d'un autre atome.  
Une paire d’électrons covalents de 2 atomes (du même matériaux ou pas) forment une liaison covalente. C'est une des principales liaisons atomiques permettant de former les éléments (cristal de Silicium, Eau (H₂O), etc...)

### Limite de conduction

Il y a une  "limite"  autour du noyau
au delà de laquelle les électrons peuvent s'éloigner du noyau et deviennent des "électrons libres".  
Si la couche de valence est au delà de cette limite, l'élément est un conducteur, sinon, si la couche de valence est en deçà de cette limite, on a affaire à un isolant (pas d'électrons libres).

Pour le silicium, on est dans le cas d'un semi-conducteur : c'est un isolant, mais sa couche de valence est très proche de la limite de conduction. 

### Cristal de silicium
Le Silicium à 4 électrons dans sa bande de valence.  
Du coup l'atome de silicium a tendance à vouloir s'associer avec 4 autres atomes de Silicium, chacun partageant un des ses électrons de valence avec lui.
Si chaque atome de silicium fait la même chose avec ses voisins, on obtient un cristal de silicium dont tous les atomes ont 8 électrons dans leur couche de valence : tous les électrons disponibles sont "mariés" dans une liaison covalente avec leurs 4 voisins.

![](images/efs_014/07_silicium_covalence.png)


### Dopage N : le phosphore
l'élément Phosphore (15 protons) suis le silicium dans le tableau périodique.
Il a un proton en plus, donc un électron en plus, et possède donc 5 électrons dans sa bande de valence.

Si dans un cristal de silicium pur bien ordonné, on remplace quelques atomes de silicium par des atomes de phosphore, on va un peu casser cette belle structure en amenant des éléments qui vont s'insérer dans le cristal en se liant aux siliciums voisins avec 4 électrons de valence,  mais en apportant aussi un électron en plus, électron qui va pouvoir aller se balader dans la bande de conduction (cf l'électron avec la flèche rouge) à cause de la nature semi-conductrice du silicium.  
![](images/efs_014/08_dopage_n.png)

### Dopage P : l'aluminium
l'élément Aluminium (13 protons) précède le silicium dans le tableau périodique.
Il a un proton en moins, donc un électron en moins, et ne possède que 3 électrons dans sa bande de valence.

Si dans un cristal de silicium pur bien ordonné, on remplace quelques atomes de silicium par des atomes d'aluminium, on va amener des éléments qui vont s'insérer dans le cristal en se liant aux siliciums voisins avec seulement 3 électrons de valence. il y a un déficit d'électron qu'on peut voir comme une charge positive en excès : un trou.


![](images/efs_014/09_dopage_p.png)


### Dopages P et N et jonction.
**Attention :** Il est a noté que le silicium non dopé, mais aussi le silicium dopé P ou N sont dans tous les cas des matériaux neutres électriquement.  
Les couches de valences peuvent avoir des électrons ou des trous en trop, mais ils sont complètement équilibrés par le nombre de protons des différents noyaux présents. (Si, P ou Al).

Quand on place un substrat dopé P (l'anode A)  a coté d'un substrat dopé N noté (la cathode K), une "diffusion" des porteurs de charges va naturellement avoir lieu au niveau de la jonction.  
Des électrons en surnombre coté N vont venir combler quelques "trous" cotés P. Ce qu'on peut aussi voir comme une diffusion de trous en excès coté P qui vont aller diffuser coté N. 
Cette diffusion de porteurs de charge crée une **zone de déplétion** au niveau de la jonction PN, qui n'est plus neutre électriquement : un champ électrique se crée entre les trous (positifs) diffusés cotés N dans la cathode et les électrons (négatifs) passés coté P dans l'anode.  
Cette zone de déplétion crée donc une tension Vka de +0.7V dans la diode.   
la valeur de 0.7V est liée au caractéristique physico-chimique du silicium, quelque soient les dopages P et N.

![](images/efs_014/10_depletion.png)

### Tension électrique appliquée à une jonction PN

#### polarisation directe et supérieure à 0.7v
Une tension directe supérieur à Vseuil entre anode (reliée au (+) de l'alim) et cathode (reliée au (-) de l'alim) amène plus de charge positives à l'anode (plus de trous), donc il est possible de "sauter la barrière" de la tension de 0.7V et le courant passe.  
On peut faire le même raisonnement en considérant un apport d'électrons à la cathode qui permet au courant d'électrons de passer la barrière (dans le sens inverse du sens conventionnel du courant)

#### polarisation inverse
En polarisation inverse (le (+) de l'alim est branché à la cathode et donc à la zone N ; le (-) de l'alim est branché à l'anode : zone P)

les électrons de la cathode sont attirés par le potentiel (+) et les trous de l'anode sont attirés par le potentiel (-) 
La zone de déplétion s'agrandit (cf. ci-dessous), le courant peut "encore moins" passer. 

![](images/efs_014/11_polarisation_inverse.png)



## Diagramme courant-tension de la diode

- Au dessus de Vth ou Vd = 0.7V pour le Silicium, la diode devient passante et laisse pratiquement passer tout le courant. (avec une pente assez raide).

- En dessous de Vth ou Vd, la diode ne laisse pas passer le courant... jusqu'à un certain point. Si on dépasse une certaine tension inverse (Vbr), se produit un avalanche et un effet Zener qui permet au courant de passer.

![](images/efs_014/12_schema_courant_tension_diode.png)

Pour le silicium, Vd=0.7V  
Pour le germanium Vd=0.3V

## Modèles de diode
Si on se focalise sur la courbe du coté "passant" de la diode, l'équation mathématique de sa forme est :
$` 
i=  I_0 (e^{\frac {V_j}{ nV_0}} -1)
`$

Avec : 
- I₀ = Courant de saturation (dépend de la diode)
- Vj = tension appliquée à la diode
- n = facteur de qualité (propre à la diode, varie en gros entre 1 et 2)
- V₀ = $`\frac {k_B T} e`$ avec : 
  - $`k_B`$ = constante de Boltzmann [![Wikipédia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Constante_de_Boltzmann)
  - T = température en Kelvin
  - e = charge d'un électron  [![wikipédia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Charge_élémentaire)


Le terme V₀ est assimilable à une tension mais dépend de la température (T).  
C'est en général quelque chose d'assez ennuyeux d'avoir un coposant dont les caratéristiques chnagent en fonction de sa témpérature...  
![](images/sad.gif)


Cette équation ne sera utilisée que si on a besoin de modéliser très précisément la diode.  
En général on peut se contenter de modèles moins précis, mais beaucoup plus simple.

### Modèle n°1 : diode parfaite

![](images/efs_014/13_modele_1.png)

la diode laisse complètement passer le courant si elle est polarisée en directe

### Modèle n°2 : tension de seuil

![](images/efs_014/14_modele_2.png)

la diode laisse complètement passer le courant si elle est polarisée en directe avec une tension supérieure à sa tension de seuil.  
On la modélise en une diode parfaite avec un petit générateur en série.


### Modèle n°3 : prise en compte d'une pente linéaire

![](images/efs_014/15_modele_3.png)

la diode laisse  passer le courant si elle est polarisée en directe, mais avec une certaine pente.  
la modélisation reprend le modèle n°2 avec une résistance en série pour prendre en compte la pente.

### Modèle n°4 : courbe réelle

![](images/efs_014/16_modele_4.png)

On utilise la relation mathématique complète pour avoir quelque chose d'extrêmement proche du réel, tenant compte de la température.



# Utilisation des diodes
## Le pont redresseur

Permet de redresser une tension sinusoïdale.  

### Simple alternance
Avec une diode simple, on supprime les alternances négatives de la sinusoïde.

### Double alternance
Le pont de Graetz permet de redresser toutes les alternances.  

![](images/efs_014/17_pont_graetz.png)

- Pour l'alternance positive : (+) en haut, (-) en bas, ce sont les diodes entourées qui sont passantes.
- Pour l'alternance négative : (-) en haut, (+) en bas, ce sont les diodes non-entourées qui sont passantes.

Dans tous les cas donc, c'est une alternance "positive qui se retrouve en sortie (courbe rouge)


C'est le "FULL BRIDGE RECTIFIER" cher à Mehdi   
[electroboom - full bridge rectifier ![youtube](images/youtube.png)](https://www.youtube.com/watch?v=sI5Ftm1-jik)  
[electroboom - diodes ![youtube](images/youtube.png) ](https://www.youtube.com/watch?v=l2y-w9aS98k) 
![](images/efs_014/18_electroboom.gif)
[A Suivre !!  ...]



## Les  diodes communes

Les "jelly beans" en électronique sont les composants qu'on a toujours dans son placard.  
Pour les diodes on trouve par exemple :
- 1N4148
- 1N914
- 1N400X
  - 1N4007 qui permet de redresser le secteur  
  ![](images/efs_014/19_diode_1N4007.png)
  
La bague peinte sur la diode correspond à la barre sur son schéma, le courant "ne peut pas entrer" par là.

Les informations à regarder pour le choix d'une diode : 
- son intensité maximum (plus haut, elle crame)
- sa tension de claquage (tension inverse maximum avant que la diode ne laisse passer tout le courant = Vbr)

## Le power Oring

Quand on souhaite alimenter un schéma avec 2 sources de tensions différentes (par exemple avec un panneau photovoltaïque et une batterie).  
Brancher les 2 sources de tensions sur le schéma est une mauvaise idée car la source la plus "forte" va passer dans l'autre (par exemple si le panneau photovoltaïque passe à l'ombre).  
![](images/efs_014/20_poweroring.png)  

Avec 2 diodes, on sécurise facilement l'alimentation la plus faible, car sa diode devient bloquée. Ainsi, seule l'alim la plus haute (minus la tension de seuil de la diode) alimentera le circuit.


## Mesurer une diode au multimètre

Passer le multimètre en mode "diode"

- Mesurée dans le "bon sens", le multimètre indique sa tension de seuil
- Mesurée dans le "mauvais sens", pas de tension, le courant ne passe pas (sinon c'est que la diode est claquée)


## Les diodes Schottky

Les diodes Schottky ont un métal (conducteur) à la place de la  zone P de la diode.

![](images/efs_014/21_schottky.png)

Les Schottky ont un comportement de diode "normal" mais elles ont quelques particularités:
- leur tension de seuil est très faible
- elles sont rapide (passage du sens bloqué au sens passant très rapide) : leur temps de commutation est très petit
- elles sont plus fragiles (caractéristiques en température diminuées)
- tension de claquage plus faible
- caractéristiques très bonnes pour les protections contre l'ESD (Electro-Static Discharge) car il faut réagir très vite.
  
  ![](images/efs_014/22_schottky_protection_esd.png)


Des références de Schottky fréquentes :
- Bat48 
- Bat45
- 1N5817


## Les diodes Zener

La diode Zener a un usage particulier.   Elle est volontairement "claquée". On va l'utiliser en polarisation inverse et utiliser l'effet Zener.  
A ses bornes en polarisation inverse, la tension est toujours la même (par construction de la diode)  
Elles peuvent aussi servir de référence de tension

![](images/efs_014/23_zener.png)


## Les diodes Electro Luminescentes (LED)

![](images/efs_014/24_led.png) ( [source](https://www.nextgenerationled.be/EN/WhyLEDlighting.html
))

Les leds sont des diodes "normales" fabriquées avec des composants bien choisi pour, en plus de leur effet de diode, émettre de la lumière quand elle sont traversées par un courant.

La couleur de la LED dépend complètement des matériaux utilisés pour la fabriquer.

![](images/efs_014/25_led_couleurs.png)  
[source](https://www.electronics-tutorials.ws/diode/diode_8.html)


Pour certaines couleurs comme le blanc, on utilise un phosphore qui réémet la longueur d'onde de la diode dans une autre longueur d'onde.  
Pour le cas du blanc, c'est une led UV qu'un phosphore réémet en blanc.

## Documentation technique d'une led
https://www.sunledusa.com/products/spec/XZM2CYK54WA-8.pdf

On y trouve 
- les "maximum ratings" (ce qu'il ne faut pas dépasser) :
  - Reverse Voltage = Tension inverse maxi = 5V
  - Forward Current = Courant direct max = 30mA
- les "operatings characteristics" : caractéristiques nominales
- La longueur d'onde (les leds émettent en général une seul longueur d'onde)
- la directivité.
  La directivité peut jouer des tours si on souhaite de l'omnidirectionnel et que vous avez de l'ultra directionnel. 
- les diagrammes tension-intensité, en fonction de la température (les leds aiment le froid)
- ... 

----
![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep015.md) nous aborderons les transistors.









