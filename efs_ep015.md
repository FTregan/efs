# EFS ep15 : Les transistors bipolaires (1)
**Épisode 15 ( 16 septembre 2022 )** 

<img src="images/efs_015/01_chat_triangle.png" alt="chat" width="300"/>

 - ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=H-lSCa5yP2U
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1592540273

# Rappel des épisodes précédents
[![youtube](images/youtube.png) (00:01:42)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1m42s) 

Nous étions passé à une nouvelle étape la semaine dernière avec notre premier semi-conducteur : la diode.

La diode est un composant qui ne laisse passer (idéalement) le courant que dans un seul sens. 
La diode n'étant pas parfaite, nous avions vu qu'elle a notamment une **tension de seuil** dépendante de ses matériaux de construction.   
Selon les besoins il y a des modèles théoriques plus ou moins "complets" pour approximer son fonctionnement.

La diode est construite avec une **jonction PN** : la juxtaposition d'un matériaux semi-conducteur dopé P et d'un matériaux semi-conducteur dopé N.  



# Le transistor bipolaire
[![youtube](images/youtube.png) (00:03:49)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=3m49s) 

Le transistor lui est, en quelque sorte, l'assemblage de deux diodes (au niveau atomique, ça ne fonctionne pas en utilisant 2 diodes "composants") : il est composé d'un sandwich de 2 jonctions : NPN ou PNP


 ## A quoi ça ressemble ?
 [![youtube](images/youtube.png) (00:04:18)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=4m18s) 

 Les transistors ont 3 pattes et existent en différents types de boitiers (suivant la puissance dissipée). Ici en boitier résine :

![](images/efs_015/02_transistor_photo1.png)
![](images/efs_015/03_transistor_photo2.png)


## le principe
[![youtube](images/youtube.png) (00:05:40)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=5m40s) 

Le transistor bipolaire est un amplificateur de courant : un très faible courant sur sa broche "base" (B,  à gauche) vers l'émetteur (flèche noire) autorisera le passage d'un fort courant (flèche bleue) entre ses broches (à droite) "collecteur" (C) et "émetteur" (E)?

![](images/efs_015/04_transistor_principe.png)

Il va pouvoir donc servir : 
- d'amplificateur linéaire (dans sa zone linéaire)
- d'interrupteur en tout ou rien  (blocage / saturation)


## les principales familles de transistors
[![youtube](images/youtube.png) (00:07:43)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=7m43s) 
- Transistors bipolaires (Bipolar Junction Transistor = BJT) 
- Transistors a effet de champ (Field Effect Transistors = FET)
  - MOSFET
  - JFET
  - <Strike>BOBAFET</strike> 

Aujourd'hui nous regarderons les bipolaires, les FET auront leurs propres épisodes.
NPN et PNP sont les 2 types de transistors bipolaires. (jonction N-P-N ou P-N-P). le PNP fonctionne sur le même principe que le NPN si ce n'est que les tensions à appliquer sont inversées pour avoir un fonctionnement similaire.  
![](images/efs_015/05_sch%C3%A9ma_npn_pnp.png)

Nous allons étudier en détail le NPN dans un premier temps.


## Le transistor NPN

[![youtube](images/youtube.png) (00:14:14)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=14m14s) 

L'**"effet transistor"** c'est ce principe qui fait qu'un petit courant $`I_{BE}`$ permet de piloter un plus gros courant $`I_{CE}`$. 

Le transistor bipolaire ressemble à 2 diodes côte à côte. Ce sont 2 jonctions 'collées'  N-P-N.  
Cependant ce n'est pas symétrique : 
- la taille des zones est différentes (la zone collecteur est plus grande).
- les dopages sont différents :
  - La zone N de l'émetteur est très fortement dopée (beaucoup d’électrons surnuméraire).
  - la zone P de la base est légèrement dopée (peu de trous surnuméraires)
  - la zone N du collecteur est modérément dopée (quelques électrons surnuméraires)

À chaque jonction se crée une barrière (une zone de déplétion)

![](images/efs_015/06_jonctions_npn.png)


### transistor ≈ 2 diodes 
Notre transistor "ressemble" a 2 diodes têtes-bêches (au niveau microscopique). L'effet transistor ne se passe que si P est physiquement très petite, ainsi qu'avec les dopages bien particuliers : ça ne marche pas avec 2 diodes "composants"

![](images/efs_015/07_npn_2_diodes.png)

Sur le 2N2222A en le posant le méplat vers le haut, les pattes sont dans l'ordre E B C. _(cf. [datasheet](https://www.mouser.com/datasheet/2/849/2n2222a-2577410.pdf) sur le site de mouser)_ 

- Si on alimente B-E avec le (+) sur la base et le (-) sur l'émetteur, on est dans le sens direct d'une diode, elle doit être passante.  
  ⇒ La mesure au multimètre, en sens direct,  (en mode diode) indique bien la tension de seuil de la diode BE.  
  Dans l'autre sens le courant ne passe pas.  
![](images/efs_015/08_mesure_diode_BE.png)  


- Si on alimente B-C avec le (+) sur la base et le (-) sur le collecteur , on est dans le sens direct de l'autre diode, elle doit être passante.  
  ⇒ La mesure au multimètre, en sens direct indique aussi la tension de seuil de la diode BC.  
  Dans l'autre sens n le courant ne passe pas.  
![](images/efs_015/09_mesure_diode_BC.png)

- si on tente de mesurer une 'diode' entre C et E, le courant ne passe pas, quelque soit le sens de mesure.  
  C'est logique, il y a au moins une des diode BE ou BC qui est en inverse selon le sens du courant.  

### Effet transistor
[![youtube](images/youtube.png) (00:28:06)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=28m06s) 

Nous avons l'habitude de raisonner en courant "conventionnel" des charges positives qui circulent entre le (+) et le (-).   
Cependant "en vrai" ce sont des électrons négatifs qui se déplacent entre le (-) et le (+)
On continuera toujours a raisonner en sens conventionnel, mais pour expliquer la physique des jonctions, il faut faire intervenir les électrons qui vont se déplacer dans l'autre sens.  
Ci-dessous la grosse flèche représentent le courant des électrons.

Quand on met une tension positive Vbe > Vseuil, les électrons arrivent par l'émetteur, et peuvent sauter la barrière : la diode BE devient passante.  Les électrons "colonisent la zone P et y remplissent les trous.  

Que se passent-il maintenant si on applique une tension positive entre le collecteur et l'émetteur.  Les électrons dans la zone P et ceux de la zone N du collecteur (fortement dopée) vont avoir tendance a être attiré par le (+) du collecteur et vont réussir à annuler la (petite) barrière  de la jonction BC (en inverse).  Un gros courant d'électron peut se mettre en place entre l'émetteur et le collecteur (soit un courant conventionnel entre le collecteur et l'émetteur).

![](images/efs_015/10_jonctions_effet_transistor.png)


Il y a une proportionnalité avec l'effet transistor :

$`i_c = \beta i_B`$

ou $`\beta`$ dépend du transistor, c'est son "gain" (parfois noté hfe). Pour un 2222 il vaut entre 75 et 300

Par ailleurs on a  : $`i_E = i_B + i_C`$

**Note :** Ces 2 relations ne fonctionnent que si les tensions Vbe et Vce sont appliquées dans le bons sens !


### Modes de fonctionnement des transistors
[![youtube](images/youtube.png) (00:40:30)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=40m30s) 

Pour polariser un transistor on a 2 tensions intéressantes :  Vbe et Vce

Avec un graphique prenant en compte Vbe en abscisses et Vbc en ordonnées, on a 4 quadrants, correspondants à 4 modes du transistor.

- Dans le quadrants en bas à gauche : 
  Vbc et Vbe sont négatifs, donc  Vb < Ve  et Vb < Vc : c'est le mode "Cut-Off" le transistor est bloqué, il ne laisse pas passer de courant


- Dans le quadrants en bas à droite :  
  Vbe positive et Vbc négative, donc Vb > Ve et Vb < Vc, ou encore Vc > Vb > Ve.  C'est le mode "forward". le transistor est passant et dans sa zone linéaire, on a une proportion entre le courant ib et le courant ic (règle du Béta).

- Dans le quadrant en haut à droite
  Vbe et Vce sont positive, donc Ve < Vb et Vc < Vb  (la base à un potentiel plus élevé que l'émetteur et que le collecteur). C'est la zone de saturation.   La vanne est ouverte en grand, on laisse tout passer, on n'est même plus proportionnel

- Dans le quadrant en haut à gauche
  Vc < Vb < Ve, c'est la zone  "reverse active". Cette zone n'a pas d'intérêt en électronique et n'est pas utilisée, on peut l'oublier.


![](images/efs_015/11_modes_transistor.png)


Les modes "saturation/cut-off" ou "saturation/blocage" sont des modes tout ou rien (binaire/numérique), tandis que le mode forward est utile pour l'amplification linéaire (analogique).


## Premiers circuit en commutation
[![youtube](images/youtube.png) (00:51:15)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=51m15s)

En commutation, on veut souvent faire de la saturation.   
La saturation est intéressante quand on veut faire du contrôle.  Par exemple pour allumer une grosse lampe qui prend 150mA.  
C'est une mauvaise idée de brancher la lampe directement sur la sortie d'un raspberry pi et d'essayer de l'allumer en passant la sortie du raspberry a 1 (schéma du haut).  
En effet, la sortie d'un raspberry pi ne doit pas tirer plus de 16mA d'après la doc.  Y brancher la lampe tirant 150mA et c'est la destruction du raspberry.  

Par contre un transistor acceptant 150mA de courant forward peut être piloté par un petit courant. (par exemple la sortie d'un raspberry pi). cf. schéma du bas.  
En branchant la pin du raspberry pi sur la base du transistor ( avec une résistance de limitation du courant), il sera possible de passer le transistor en saturation pour qu'il puisse faire passer un maximum de courant dans la lampe branchée, elle sur le collecteur (avec aussi une résistance de limitation du courant).


![](images/efs_015/12_pilotage_charge.png)

Avec un $`\beta`$ d'environ 100, on peut piloter 20mA (pour allumer une diode par exemple) avec un courant de base de 0.2mA

En saturation Vce est très petit (~ 0.2V). il faut que Vb > Vc, mais Vce étant petit, et Ve étant 0V, c'est assez simple à réaliser.
On prend en général un facteur de sécurité de 2 (on multiplie le courant Ib de saturation par 2 ) pour être sûr de bien saturer le transistor.


## Le mode saturation
[![youtube](images/youtube.png) (00:59:48)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=59m48s)

C'est considéré comme un fil (interrupteur fermé)

N'existe que si et seulement si :
- Vb > Ve
- Vb > Vc

Selon les transistors, en saturation, Vce sat = 0.05 à 0.2V (voir la datasheet)

La tension Vbe doit dépasser le seuil de la diode : Vbe ≥ Vth (notée parfois Vd)  (voir la datasheet du transistor), mais c'est la tension de seuil d'une diode donc ~ 0.6-0.7v

## Le mode Cut-off  (bloqué)
[![youtube](images/youtube.png) (01:04:41)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h4m41s)

C'est considéré comme un circuit ouvert (interrupteur ouvert)

N'existe que si et seulement si :
- Vb < Ve  (en fait Vbe < Vseuil)
- Vb < Vc

Dans ce mode, Ic = 0


## Le mode forward (amplification)
[![youtube](images/youtube.png) (01:07:18)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h7m18s) 

DAns ce mode, le transistor est en mode amplification linéaire

En théorie, on calcul l'intensité du collecteur, on divise par $`\beta`$ et on obtient Ib. Ensuite on calcule les résistance pour faire passer cet Ib dans la base.  

Cependant, le $`\beta`$ d'un transistor est tout sauf stable et précis. (il peut varier du simple au double pour une même série, il varie fortement avec la température, la polarisation... ).

Il faut donc préférer une méthode de polarisation du transistor (pour le mettre en mode forward ou linéaire) qui ne dépend pas du $`\beta`$. 

Nous étudierons ça dans le prochain épisode. Pour le moment nous restons sur le mode saturation



## Calcul des valeurs composants pour le mode saturation (pilotage en tout ou rien)
[![youtube](images/youtube.png) (01:09:51)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h9m51s) 

On a une diode à allumer avec un raspberry pi. on veut que la diode utilise 20mA ce qui est au dessus des 16mA max en sortie du raspberry.

On utilise donc un transistor qu'on va mettre en saturation pour piloter cet allumage de diode.

En saturation Vcesat est proche de 0v (0.3V selon la datasheet du 2222A) . On approxime donc Vce a 0.3V, soit 4.7V dans Ur + Ud.  Ud est d'environ 2V pour une led, ce qui laisse 2.7V pour Ur, avec 20mA de courant, soit 2.7/0.020 = 135ohm.

On vérifie, au cas ou, que les 20mA qui vont circuler dans le collecteur sont supporté par le transistor. (voir la datasheet). C'est OK

Pour dimensionner la partie de gauche, prenons un béta très pessimiste (disons 75). Si on veut 20mA dans le collecteur, on doit avoir 0.02/75=0.3mA  dans la base.  
Pour être en saturation Vb doit etre > Ve (soit > 0) et supérieur a Vc (soit > 0.3V). Mais on veut aussi qu'un courant passe (Ibe>0)  donc on doit etre supérieur a la tension de seuil de la diode BE (> 0.7v).  
Vb doit donc etre a 0.7V, ce qui laisse 3.3-0.7V ) 2.6V pour La tension aux bornes de Rb (avec un courant de 0.3mA x 2 = 0.6mA , x2 comme facteur de sécurité pour bien saturer). Soit une résistance de 2.6/0.6 = 4.3k

Avec les 2 résistance calculée, on aura un transistor en saturation quand la sortie du raspberry sera à 3.3V (1), donc la led sera allumée.
Avec 0 en sortie du raspberry, le transistor sera bloqué et la diode sera éteinte.

**Attention,** dans ce genre de montage, il ne faut pas mettre de résistance derrière l'émetteur : il est relié à la masse.


## Montages classiques en saturation : la logique booléenne
[![youtube](images/youtube.png) (01:18:45)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h18m45s) 

###  Porte NOT (NON)
[![youtube](images/youtube.png) (01:19:00)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h19m0s) 

![](images/efs_015/13_not.png)

- Quand on applique une tension a la base (1 logique) 
  - le transistor passe en saturation.  
    - Un courant de base se met en place, 
    - donc un gros courant de collecteur passe, 
    - donc la tension aux bornes de Rc devient très importante, 
    - donc la sortie est ramenée au 0V

- Quand on n'a pas de tension sur la base (état logique 0)
  - il n'y a pas de courant de base,
  - donc pas de courant collecteur,
  - donc 0v aux bornes de Rc
  - donc la sortie est remontée à Vcc (état 1)
  
Ce montage a été utilisé très longtemps comme porte NOT dans les circuits TTL.

###  Porte AND (ET)
[![youtube](images/youtube.png) (01:22:09)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h22m09s) 

On a un ET logique quand la sortie est à 1 uniquement quand les 2 entrées sont à 1

![](images/efs_015/14_and.png)

- Si les 2 transistors sont saturés (2 entrées à Vcc ou 1 logique), le courant passe dans les collecteurs et dans la résistance émetteur du bas. 
Qui dit gros courant de résistance dit Ur ≈ Vcc, donc la sortie est à 1

- Si un ou 2 transistors sont bloqués (une ou 2 entrée à 0, Ir=0 donc Ur=0, donc la sortie est à 0).


###  Porte OR (OU)
[![youtube](images/youtube.png) (01:24:44)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h24m44s) 

Contrairement au ET, on a besoin d'une seule entrée de la porte OU à 1 pour que la sortie passe à 1. les transistors sont donc placés en parallèle.

![](images/efs_015/15_or.png)

- Si un des transistor est saturé (A ou B à 1), un gros courant passe dans Re, et Ur passe a Vcc et donc la sortie à 1
- si aucun des transistor n'est saturé (A et B = 0), ie =0 donc Ue=0, la sortie est à 0


### Les différents types de portes logiques
- TTL  : c'est ce qu'on vient de voir, la logique TTL est réalisée avec des transistors bipolaires
- CMOS : plus récente, ces portes logiques sont réalisées avec des transistors à effet de champs (mosfet)


## Autre circuit classique en saturation : Le pont en H (H bridge)
[![youtube](images/youtube.png) (01:28:56)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h28m56s) 

Ce circuit utilises des NPN et des PNP.
Le PNP se comporte comme le NPN si on change tous les signes du diagramme en quadrants.

Il permet de faire tourner un moteur dans un sens ou dans un autre, ou d'inverser le sens du passage du courant dans un circuit/composant.

![](images/efs_015/16_pont_h.png)

- Quand on positionne un "1" à gauche et un "0" à droite :
  - le "1" à gauche rend :
    - Le transistor NPN du bas à gauche passant (saturé)
    - Le transistor PNP du haut à gauche bloqué (circuit ouvert)
  - le "0" à droite rend : 
    - le transistor PNP en haut à droite passant (saturé) 
    - le transistor NPN du bas à droite bloqué (ouvert)
  ⇒ le courant passe dans le moteur de la droite vers la gauche.

- Dans l'autre sens, quand on positionne un "0" à gauche et un "1" à droite :
  - le "0" à gauche rend :
    - Le transistor NPN du bas à gauche bloqué (ouvert)
    - Le transistor PNP du haut à gauche saturé (passant)
  - le "1" à droite rend : 
    - le transistor PNP en haut à droite bloqué (ouvert) 
    - le transistor NPN du bas à droite saturé (fermé)
  ⇒ le courant passe dans le moteur de la gauche vers la droite.


Les transistors des ponts en H peuvent devoir faire passer un courant important quand il s'agit d'alimenter un "gros" moteur. Ce sont donc souvent des composants discrets, avec possiblement des radiateurs pour la dissipation de chaleur.  
![](images/efs_015/17_pont_h_externeC.png)  

Les "pololu", drivers des moteurs pas à pas d'imprimantes 3D par exemple, ont leur pont en H  inclus dans une seule puce mais ne peuvent pas faire passer un courant très important, et chauffent rapidement. (d'où les radiateurs souvent montés pour les refroidir).  
![](images/efs_015/18_pololu.png)


# Appartée : discussion sur les moteurs de drones et de robots ainsi que de réducteurs  haut de gamme
[![youtube](images/youtube.png) (01:42:51)](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=1h42m51s) 

brushless, maxon, pancake, réducteur harmonic drive....

Hors sujet à voir en vidéo uniquement...



# Conclusion : mort d'un transistor par surtension
[![youtube](images/youtube.png) (02:02:35](https://www.youtube.com/watch?v=H-lSCa5yP2U&t=2h02m35s)

En appliquant une grosse tension sur la jonction base-émetteur (ou base-collecteur), le transistor va brûler..

![](images/efs_015/19_burn.png) 


----

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep016.md) nous étudierons un montage de transistor en commutation "en vrai", avant de passer au transistor bipolaire en mode amplificateur.







