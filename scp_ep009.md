# Soirée Chill et Projets ep09 : Le 555, ou pourquoi je m'en balance

**Épisode 09 ( 11 août 2022 )** 

<img src="images/scp_009/01_chat_balance.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=y10nMkYF_2o
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1558988338

# Présentation

Le NE 555  [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/NE555) est un circuit intégré créé en 1970.  
C'est un circuit de temporisation qui peut fonctionner en monostable, astable et bistable.  
Ce circuit est bien connu des électroniciens amateurs car il est simple à utiliser et permet facilement de faire des petits montages d'initiation.

Le 555 est un circuit à 8 pattes

![](images/scp_009/03_555_pins.png)

- Vcc (tension d'alimentation) peut aller de 4.5v à 16V environ
- Reset est actif à l'état bas ($`\overline{RESET}`$)

# Schéma interne du 555

[![youtube](images/youtube.png) (00:00:00)](https://www.youtube.com/watch?v=y10nMkYF_2ot=0h00m00s)  

![](images/scp_009/02_555_timer_block_diagram.png)

Le 555 est composé, en interne :
- d'un pont diviseur de 3 résistances de mêmes valeurs (5k)
- de 2 comparateurs
- d'une bascule RS
- d'un transistor de décharge
- d'un inverseur en sortie

## Le pont diviseur
Le pont diviseur de 3 résistances égales, entre vcc et le ground, permet d'avoir des références de tension pour les comparateurs.  en effet, aux nœuds de jonctions entre les résistances on a :
- 1/3 Vcc entre la résistance du bas et celle du milieu (point relié au (+) du comparateur du bas))
- 2/3 Vcc entre la résistance du milieu et celle du haut (point relié au (-) du comparateur du haut)

## Les comparateurs
Un comparateur mesure et compare des tensions entre ses 2 entrées.  
Sa sortie est binaire (1 ou 0) selon le résultat de la comparaison :
- si la tension appliquée à la borne (+) d'un comparateur est supérieure à celle appliquée à la borne (-), alors la sortie du comparateur = 1
- si la tension appliquée à la borne (+) d'un comparateur est inférieure à celle appliquée à la borne (-), alors la sortie du comparateur = 0

Un comparateur travaille en tension : aucun courant ne rentre par les entrées, et aucun courant "fort" ne sort en sortie.

Dans le 555, chaque comparateur pilote une entrée de la bascule RS

### Comparateur pilotant R
Le comparateur du haut passe à l'état haut quand la tension à sa borne (+) passe au dessus de la tension à sa borne (-). Donc quand  THRESHOLD > 2/3 Vcc.    
- THRESHOLD > 2/3 Vcc  ⇒  sortie comparateur = 1
- THRESHOLD < 2/3 Vcc  ⇒  sortie comparateur = 0

### Comparateur pilotant S
Le comparateur du bas passe à l'état haut quand la tension à sa borne (-) passe en dessous de la tension à sa borne (+). Donc quand  TRIGGER < 1/3 Vcc.    
- TRIGGER < 1/3 Vcc  ⇒  sortie comparateur = 1
- TRIGGER > 1/3 Vcc  ⇒  sortie comparateur = 0

## La bascule RS
Une bascule RS (un flip fop) est un circuit à la base de la notion de mémoire.

Il a 2 entrées R et S et une sortie Q (ainsi souvent qu'une sortie inverse de Q : $`\overline{Q}`$)

La bascule tranforme des impulsions courtes en état "permanents". C'est une mémoire.

Quand R = 0 et S = 0  ⇒ Q ne change pas d'état
Quand R = 1 et S = 0  ⇒ Q passe à 0 (et y reste)
Quand R = 0 et S = 1  ⇒ Q passe à 1 (et y reste)


R  \ S|  1          |  0 
:---  | :---:       | :---: 
1     | interdit    | Q passe à 0 
0     | Q passe à 1 | Q ne chnage pas

Rappel : $`\overline{Q} =\  !Q =not(Q)`$ 

## Le transistor de décharge

Quand $`\overline{Q} = 1`$  donc quand Q = 0  (donc quand THRESHOLD est passé, à un moment, au dessus  de 2/3 de Vcc), le transistor est "saturé" (un courant passe dans sa base), il se comporte donc comme un interrupteur fermé (Vce ~= 0).  
La pin 7 (DISCHARGE) est donc reliée à la masse

À l'inverse, quand $`\overline{Q} = 0`$  donc quand Q = 1, le transistor est "bloqué" et se comporte comme un interrupteur ouvert.  DISCHARGE n'est pas relié à la masse.

## L'inverseur en sortie
Pour piloter le transistor comme on le souhaite, c'est $`\overline{Q} = 1`$ qui est utilisé comme sortie de la bascule. Pour tout remettre dans l'ordre pour la sortie du 555, le signal est de nouveau inversé.


# Le 555 en bi-stable

![](images/scp_009/05_555_bistable.svg)

- Une led est branchée en sortie via une résistance de limitation de courant.
- La broche $`\overline{Reset}`$ est branchée à Vcc, via une résistance de pull up, et un switch relié à la masse permet de passer cette broche à 0 en appuyant dessus.
- Idem pour le Trigger qui est relié à Vcc par un pull up avec un switch relié à la masse permettant de passer le trigger à 0V.

On a ici un montage bi-stable.  
Si on appuie, même rapidement sur Ⓢ  : le trigger passe sous 1/3 de Vcc, donc le comparateur du bas active le S de la bascule, la sortie du 555 passe à 1 et la led s'allume.  
Le seul moyen de l'éteindre est d'appuyer sur Ⓡ  qui va "resetter" la bascule et repasser la sortie à 0 : la led s'éteint.


# Le 555 en monostable
![](images/scp_009/06_555_monostable.svg)

- Une led est branchée en sortie via une résistance de limitation de courant. (idem au monostable)
- La broche Threshold est branchée à Vcc via une résistance de pull up, à un condensateur relié à la masse par son autre broche, ainsi qu'à la broche Discharge.
- Le Trigger qui est relié à Vcc par un pull up avec un switch relié à la masse permettant de passer le trigger à 0V. (idem au monostable).

Ce montage est un **"monostable"** :  un appui court sur l'interrupteur Ⓢ déclenche l'allumage d'une led ainsi qu'une minuterie dépendant de la constante de temps RC des composants reliés au Threshold.  
Au bout d'un certain temps, la minuterie éteint la led et se remet à 0 automatiquement, "attendant" un éventuel réappui sur Ⓢ.


## À l'allumage du circuit,
- Q = 0 donc $`\overline{Q} = 1`$.
- Out = 0 : la led est éteinte
- Le transistor de décharge est saturé et se comporte comme un circuit fermé, reliant Discharge à la masse (ainsi donc que le (+) du comparateur R, ainsi que la broche "positive" du condensateur).  
- Le condensateur ne peut donc pas se charger car il est court-circuité à la masse. De plus, le comparateur R sort 0 car V+ (OV) < V- (2/3 Vcc).  
- Le circuit est stable, Out = 0 et la led est éteinte.



## Appuyons brièvement sur Ⓢ 
- Trigger passe à la masse pendant l'appui sur Ⓢ
- Le comparateur S change d'état car v- (0V) < 1/3 Vcc, 
- La bascule est "settée" : Q passe à 1 et $`\overline{Q} = 0`$
- out passe donc à 1 : la led s'allume
- Le transistor de décharge se bloque et coupe la connection de Discharge à la masse
- V+ du comparateur R passe donc à Vcc (le condensateur est déchargé donc Uc = 0V)
- Le condensateur commence à se charger par la résistance.
- Threshold est relié au + du condensateur qui est en train de se charger. pour le moment Uc est inférieur à 2/3 Vcc. Donc R reste à 0. 
- le bouton Ⓢ a été relaché (appui bref), la led est allumée, et Uc augmente petit a petit.


## Uc passe au dessus des 2/3 Vcc
- Uc aux borne du condensateur atteint 2/3 de Vcc au bout d'un certain temps dépendant de la constante de temps RC.
- À ce moment là le comparateur R bascule sa sortie à 1, activant le Reset de la bascule
- Q passe à 0 et $`\overline{Q} = 1.
- Out passe à 0, la led s'éteint.
- le transistor de décharge redevient passant court-circuitant la condensateur à la masse. Le condensateur se décharge brusquement à la masse via la broche Discharge.
- La tension Uc repasse à 0, le comparateur R repasse sa sortie à 0.
- On est de nouveau dans le cas de départ : led éteinte, condensateur déchargé et court-circuité empêchant son rechargement.


## Schéma temporel
![](images/scp_009/07_555_monostable_temporel.svg)


# Le 555 en multivibrateur astable
![](images/scp_009/08_555_astable.svg)

- Une led est branchée en sortie via une résistance de limitation de courant. (idem au monostable)
- Les broches Threshold et Trigger sont branchées à Vcc via deux résistances, et à un condensateur relié à la masse par son autre broche
- Le point entre R1 et R2 est relié à Discharge

Ce montage est un **"multivibrateur astable"** :  un oscillateur basculant automatiquement entre 0 et 1 et dont le temps "haut" et le temps "bas" sont réglables par le choix de R1, R2 et C (R1+R2)xC pour le temps "haut" et R2xC pour le temps "bas".


## À l'allumage du circuit
- Le condensateur n'est pas chargé donc Uc=0.
- La tension du trigger étant inférieure à Vcc, Set est actif
- Q passe donc à 1, et $`\overline{Q}`$ à 0.
- Out passe à 1, la led s'allume.
- Le transistor de décharge et bloqué, le point entre R1 et R2 est "en l'air".
- le condensateur peut donc commencer à se charger via R1 et R2

## Uc atteint 2/3 Vcc
- Au moment ou Uc (donc Trigger et Threshold) passe au délà de 2/3 Vcc. le Reset de la bascule est activée, Q passe à 0 et $`\overline{Q}`$ à 1.
- Out passe à 0, la led s'éteint
- le transistor de décharge se sature et relie Discharge à la masse.
- le condensateur commence à se décharger via R2 dans la broche 7 du 555.
  
## Uc descent sous  1/3 Vcc
- Au moment ou Uc (donc trigger et Threshold) passe en dessous de 1/3 Vcc. le Set de la bascule est activée
- Q passe à 1 et $`\overline{Q}`$ à 0.
- Out passe à 1, la led s'allume
- le transistor de décharge se bloque et déconnecte Discharge de la masse.
- le condensateur commence de nouveau à se charger via R1 et R2.
- On recommence un cycle.
  
## Schéma temporel
![](images/scp_009/09_555_astable_temporel.svg)


# calcul des valeurs de RC pour le 555

https://ohmslawcalculator.com

## Pour le monostable
https://ohmslawcalculator.com/555-monostable-calculator

## Pour le multivibrateur astable
https://ohmslawcalculator.com/555-astable-calculator


# Et la patte 5 "Control Voltage" ?
C'est la seul boche du 555 que nous n'avons pas utilisé dans les 3 montages de bases.

Elle permet d'ammener, comme référence de tension des comparateurs, une tension autre que 2/3 de Vcc.
Ainsi les comparateurs pourront se déclencher si le Threshold passe au dessus de VControl ou si le trigger passe en dessous de VControl/2.

C'est utile notamment pour fabriquer des VCO (des oscillateurs dont la fréquence est commandée par une tension) :
http://www.learningaboutelectronics.com/Articles/Voltage-controlled-oscillator-VCO-circuit-with-a-555-timer.php




----

![fin](images/thats_all_folks_porky.png)












